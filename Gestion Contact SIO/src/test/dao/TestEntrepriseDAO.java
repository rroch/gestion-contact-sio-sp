package test.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import modele.dao.EntrepriseDAO;
import modele.dao.Jdbc;
import modele.metier.Entreprise;

/**
 * Test EntreprisetDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestEntrepriseDAO {

    public static void main(String[] args) throws SQLException {
        java.sql.Connection cnx = null;
        try {
            System.out.println("\n === Test DAO Entreprise === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            
            test3_Insert(999, "SOFTYLAB", "02-40-52-68-94", "Rue de Clisson", 44300, "Clisson");
            System.out.println("Test 3 effectué : insertion\n");
            test4_Update(999, "SOFTYLOUB", "06.85.85.49.63", "Rue de Rouen", 28630, "Morancez");
            System.out.println("Test 4 effectué : mise à jour\n");
            test5_Delete(999);
            System.out.println("Test 5 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }
    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV"); 
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche un entreprise d'après son identifiant
     *
     * @param idEntreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int idEntreprise) throws SQLException {
        Entreprise cetEntreprise = EntrepriseDAO.selectOne(idEntreprise);
        if (cetEntreprise != null) {
            System.out.println("Entreprise d'identifiant " + idEntreprise + " : " + cetEntreprise.toString());
        } else {
            System.out.println("L'entreprise d'identifiant " + idEntreprise + " n'existe pas ");
        }

    }

    /**
     * Affiche toutes les entreprises
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test2_SelectMultiple() throws SQLException {
        ArrayList<Entreprise> desEntreprises = (ArrayList<Entreprise>) EntrepriseDAO.selectAll();
        System.out.println("Les entreprises lus : " + desEntreprises.toString());
    }

    /**
     * Ajoute une entreprise
     *
     * @param idEntreprise
     * @param nomEntreprise
     * @param numTelEntreprise
     * @param adrEntreprise
     * @param CPEntreprise
     * @param villeEntreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int idEntreprise, String nomEntreprise, String numTelEntreprise, String adrEntreprise, int CPEntreprise, String villeEntreprise) throws SQLException {
        int nb = EntrepriseDAO.insert(idEntreprise, nomEntreprise, numTelEntreprise, adrEntreprise, CPEntreprise, villeEntreprise);
        System.out.println("Une nouvelle Entreprise a été inséré: " + nb);
        test2_SelectMultiple();
    }

    /**
     * Modifie une entreprise
     *
     * @param idEntreprise
     * @param nomEntreprise
     * @param numTelEntreprise
     * @param adrEntreprise
     * @param CPEntreprise
     * @param villeEntreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Update(int idEntreprise, String nomEntreprise, String numTelEntreprise, String adrEntreprise, int CPEntreprise, String villeEntreprise) throws SQLException {
        int nb = EntrepriseDAO.update(idEntreprise, nomEntreprise, numTelEntreprise, adrEntreprise, CPEntreprise, villeEntreprise);
        System.out.println("L'entreprise " + idEntreprise + " a été mis à jour: " + nb);
        test1_SelectUnique(idEntreprise);
    }

    /**
     * Supprime une entreprise
     *
     * @param idEntreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */ 
    public static void test5_Delete(int idEntreprise) throws SQLException {
        int nb = EntrepriseDAO.delete(idEntreprise);
        System.out.println("Une entreprise a été supprimée: " + nb);
        test2_SelectMultiple();
    }
}
