package test.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Stage;
import modele.dao.Jdbc;
import modele.dao.StageDAO;

/**
 * Test StageDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestStageDAO {

    public static void main(String[] args) {
        java.sql.Connection cnx = null;
        try {
            System.out.println("\n === Test DAO Stage === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : Connexion\n");
            test1_SelectUnique(2);
            System.out.println("Test 1 effectué : Sélection unique\n");
            test21_SelectMultiple();
            System.out.println("Test 2.1 effectué : Sélection multiple\n");
            test22_SelectByEntreprise(2);
            System.out.println("Test 2.2 effectué : Sélection par entreprise\n");
            test23_SelectByEleve(2);
            System.out.println("Test 2.3 effectué : Sélection par élève\n");
            test24_SelectListeStageParAnnee(2019, 2020);
            System.out.println("Test 2.4 effectué : Sélection d'une liste de stage\n");
            test25_SelectListeStageParAnneeAndSpe(2019, 2020, "SLAM");
            System.out.println("Test 2.5 effectué : Sélection d'une liste de stage par année et spécialité\n");
            test26_SelectListeStagiaireParPro("GASTON");
            System.out.println("Test 2.6 effectué : Sélection d'une liste de stagiaire par professionnel\n");
            test27_SelectListeStagiaireParEntre("Mercer OIUT");
            System.out.println("Test 2.7 effectué : Sélection d'une liste de stagiaire par entreprise\n");
            test3_Insert(999, 2020, 7, "Sujet Java", 2, 5, 5, 1);
            System.out.println("Test 3 effectué : Insertion\n");
            test4_Update(999, 2020, 7, "Sujet PHP", 2, 5, 5, 2);
            System.out.println("Test 4 effectué : Modification\n");
            test5_Delete(999);
            System.out.println("Test 5 effectué : Suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }
    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV"); 
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche un Stage d'après son identifiant
     *
     * @param idStage Identifiant du Stage
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int idStage) throws SQLException {
        Stage ceStage = StageDAO.selectOne(idStage);
        if (ceStage != null) {
            System.out.println("Stage d'identifiant : " + idStage + " : " + ceStage.toString());
        } else {
            System.out.println("La stage d'identifiant : " + idStage + " n'existe pas ");
        }
    }

    /**
     * Affiche toutes les stages
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test21_SelectMultiple() throws SQLException {
        ArrayList<Stage> desStages = StageDAO.selectAll();
        System.out.println("Les stages lus : " + desStages.toString());
    }

    /**
     * Affiche tous les stages par rapport à une entreprise
     *
     * @param idEntreprise Identifiant de l'entreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test22_SelectByEntreprise(int idEntreprise) throws SQLException {
        ArrayList<Stage> desStages = StageDAO.selectStageByEntreprise(idEntreprise);
        if (desStages != null) {
            System.out.println("Les stages de l'entreprise N°" + idEntreprise + " : " + desStages.toString());
        } else {
            System.out.println("La table stage ne contient aucun stage ayant pour identifiant entreprise : " + idEntreprise);
        }
    }

    /**
     * Affiche toutes les stages par rapport à une entreprise
     *
     * @param idEleve Identifiant de l'élève
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test23_SelectByEleve(int idEleve) throws SQLException {
        ArrayList<Stage> desStages = StageDAO.selectStageByEntreprise(idEleve);
        if (desStages != null) {
            System.out.println("Les stages de l'éleve " + idEleve + " : " + desStages.toString());
        } else {
            System.out.println("La table visite est nulle");
        }
    }

    /**
     * Affiche tous les Stages par rapport a
     * deux année donnée
     *
     * @param AnneeDebut
     * @param AnneeFin
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test24_SelectListeStageParAnnee(int AnneeDebut, int AnneeFin) throws SQLException {
        ArrayList<Stage> desStages = (ArrayList<Stage>) StageDAO.selectStageByAnnee(AnneeDebut, AnneeFin);
        System.out.println("Les stages lus : " + desStages.toString());
    }

    /**
     * Affiche tous les Stage par Specialite et a
     * deux année donnée
     *
     * @param AnneeDebut
     * @param AnneeFin
     * @param spe
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test25_SelectListeStageParAnneeAndSpe(int AnneeDebut, int AnneeFin, String spe) throws SQLException {
        ArrayList<Stage> desStages = (ArrayList<Stage>) StageDAO.selectStageByAnneeAndSpe(AnneeDebut, AnneeFin, spe);
        System.out.println("Les stages lus : " + desStages.toString());
    }

    /**
     * Affiche tous les stages selon un professionnel
     *
     * @param pro
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test26_SelectListeStagiaireParPro(String pro) throws SQLException {
        ArrayList<Stage> desStages = (ArrayList<Stage>) StageDAO.selectStagiaireByPro(pro);
        System.out.println("Les stages lus : " + desStages.toString());
    }

    /**
     * Affiche tous les stages selon une entreprise
     *
     * @param entr
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test27_SelectListeStagiaireParEntre(String entr) throws SQLException {
        ArrayList<Stage> desStages = (ArrayList<Stage>) StageDAO.selectStagiaireByEntre(entr);
        System.out.println("Les stages lus : " + desStages.toString());
    }
    
    /**
     * Ajoute un stage dans la BDD
     *
     * @param idStage Identifiant du Stage
     * @param anneeStage Annee du Stage
     * @param periodeStage Période de stage (en semaine)
     * @param sujetStage
     * @param idEntreprise Identifiant de l'entreprise du stage
     * @param idEleve Identifiant de l'élève stagiaire
     * @param idProfessionnel Identifiant du professionnel
     * @param idTechno
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int idStage, int anneeStage, int periodeStage, String sujetStage, int idEntreprise, int idEleve, int idProfessionnel, int idTechno) throws SQLException {
        int nb = StageDAO.insert(idStage, anneeStage, periodeStage, sujetStage, idEntreprise, idEleve, idProfessionnel, idTechno);
        System.out.println("Un nouveau stage a été inséré: " + nb);
        test21_SelectMultiple();
    }

    /**
     * Modifie un stage dans la BDD
     *
     * @param idStage Identifiant du Stage
     * @param anneeStage Annee du Stage
     * @param periodeStage Période de stage (en semaine)
     * @param sujetStage
     * @param idEntreprise Identifiant de l'entreprise du stage
     * @param idEleve
     * @param idProfessionnel
     * @param idTechno
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Update(int idStage, int anneeStage, int periodeStage, String sujetStage, int idEntreprise, int idEleve, int idProfessionnel, int idTechno) throws SQLException {
        int nb = StageDAO.update(idStage, anneeStage, periodeStage, sujetStage, idEntreprise, idEleve, idProfessionnel, idTechno);
        System.out.println("Le stage " + idStage + " a été mis à jour: " + nb);
        test1_SelectUnique(idStage);
    }

    /**
     * Supprime un stage dans la BDD
     *
     * @param idSpecialite Identifiant du Stage
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test5_Delete(int idSpecialite) throws SQLException {
        int nb = StageDAO.delete(idSpecialite);
        System.out.println("Un stage a été supprimée: " + nb);
        test21_SelectMultiple();
    }
}
