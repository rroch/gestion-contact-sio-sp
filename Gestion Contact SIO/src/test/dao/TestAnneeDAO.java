package test.dao;

import java.sql.SQLException;
import java.util.List;
import modele.dao.AnneeDAO;
import modele.dao.Jdbc;
import modele.metier.Annee;

/**
 * Test AnneeDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestAnneeDAO {

    public static void main(String[] args) {

        java.sql.Connection cnx = null;

        try {
            System.out.println("\n === Test DAO Annee === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(2015);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            test3_Insert(2050);
            System.out.println("Test 3 effectué : insertion\n");
            test4_Delete(2050);
            System.out.println("Test 4 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV"); 
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche une année d'après son identifiant
     *
     * @param dateAnnee une année
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int dateAnnee) throws SQLException {
        Annee cetAnnee = AnneeDAO.selectOne(dateAnnee);
        if (cetAnnee != null) {
            System.out.println("Année d'identifiant : " + dateAnnee + " : " + cetAnnee.toString());
        } else {
            System.out.println("L'année d'identifiant : " + dateAnnee + " n'existe pas ");
        }

    }

    /**
     * Affiche toutes les années
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test2_SelectMultiple() throws SQLException {
        List<Annee> desAnnees = AnneeDAO.selectAll();
        System.out.println("Les années lues : " + desAnnees.toString());
    }

    /**
     * Ajoute une année
     *
     * @param dateAnnee une année
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int dateAnnee) throws SQLException {
        Annee uneAnnee = new Annee(dateAnnee);
        int nb = AnneeDAO.insert(uneAnnee);
        System.out.println("Une nouvelle année a été insérée: " + nb);
        test2_SelectMultiple();
    }

    /**
     * Supprime une Annee
     *
     * @param dateAnnee une année
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Delete(int dateAnnee) throws SQLException {
        int nb = AnneeDAO.delete(dateAnnee);
        System.out.println("Une année a été supprimée: " + nb);
        test2_SelectMultiple();
    }

}
