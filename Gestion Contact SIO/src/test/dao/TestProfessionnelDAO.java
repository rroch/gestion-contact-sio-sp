package test.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import modele.dao.Jdbc;
import modele.dao.ProfessionnelDAO;
import modele.metier.Professionnel;

/**
 * Test ProfessionnelDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestProfessionnelDAO {

    public static void main(String[] args) throws SQLException {
        java.sql.Connection cnx = null;
        try {
            System.out.println("\n === Test DAO Professionnel === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test 1 effectué : sélection unique\n");
            test21_SelectMultiple();
            System.out.println("Test 2.1 effectué : sélection multiple\n");
            test22_SelectMultipleAccordJuryParAnnee(2019, 2020);
            System.out.println("Test 2.2 effectué : sélection Multiple AccordJury Par Annee\n");
            test3_Insert(999, "LEMOAL", "Cedric", "SLAM", "softylab@gmail.com", "02.40.52.36.95", 5);
            System.out.println("Test 3 effectué : insertion\n");
            test4_Update(999, "LEMOAL", "Cedric", "SISR", "softylab@gmail.com", "02.40.52.36.95");
            System.out.println("Test 4 effectué : mise à jour\n");
            test5_Delete(999);
            System.out.println("Test 5 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }
    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV"); 
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche un professionnel d'après son identifiant
     *
     * @param idProfessionnel
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int idProfessionnel) throws SQLException {
        Professionnel ceProfessionnel = ProfessionnelDAO.selectOne(idProfessionnel);
        if (ceProfessionnel != null) {
            System.out.println("Professionnel d'identifiant : " + idProfessionnel + " : " + ceProfessionnel.toString());
        } else {
            System.out.println("Le professionnel d'identifiant : " + idProfessionnel + " n'existe pas ");
        }

    }

    /**
     * Affiche tous les professionnels
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test21_SelectMultiple() throws SQLException {
        ArrayList<Professionnel> desProfessionnels = (ArrayList<Professionnel>) ProfessionnelDAO.selectAll();
        System.out.println("Les professionnels lus : " + desProfessionnels.toString());
    }

    /**
     * Affiche tous les professionnels qui ont donner leurs accord Jury entre
     * deux année donnée
     *
     * @param AnneeDebut
     * @param AnneeFin
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    
    public static void test22_SelectMultipleAccordJuryParAnnee(int AnneeDebut, int AnneeFin) throws SQLException {
        ArrayList<Professionnel> desProfessionnels = (ArrayList<Professionnel>) ProfessionnelDAO.selectProfByAccordAnnee(AnneeDebut, AnneeFin);
        System.out.println("Les professionnels lus : " + desProfessionnels.toString());
    }
    
    /**
     * Ajoute un Professionnel
     *
     * @param idProfessionnel Identifiant du professionnel
     * @param nomProf Nom du professionnel
     * @param prenomProf Prenom du professionnel
     * @param specialiteProf Spécialité du professionnel
     * @param mailProf email du Professionnel
     * @param numeroTelProf téléphone du professionnel
     * @param idEntreprise Identifiant de l'entreprise du professionnel
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int idProfessionnel, String nomProf, String prenomProf, String specialiteProf, String mailProf, String numeroTelProf, int idEntreprise) throws SQLException {
        int nb = ProfessionnelDAO.insert(idProfessionnel, nomProf, prenomProf, specialiteProf, mailProf, numeroTelProf, idEntreprise);
        System.out.println("Un nouveau Professionnel a été inséré: " + nb);
        test21_SelectMultiple();
    }

    /**
     * Modifie un Professionnel
     *
     * @param idProfessionnel Identifiant du professionnel
     * @param nomProf Nom du professionnel
     * @param prenomProf Prenom du professionnel
     * @param specialiteProf Spécialité du professionnel
     * @param mailProf email du Professionnel
     * @param numeroTelProf téléphone du professionnel
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Update(int idProfessionnel, String nomProf, String prenomProf, String specialiteProf, String mailProf, String numeroTelProf) throws SQLException {
        int nb = ProfessionnelDAO.update(idProfessionnel, nomProf, prenomProf, specialiteProf, mailProf, numeroTelProf);
        System.out.println("Le professionnel " + idProfessionnel + " a été mis à jour: " + nb);
        test1_SelectUnique(idProfessionnel);
    }

    /**
     * Supprime un professionnel
     *
     * @param idProfessionnel Identifiant du professionnel
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test5_Delete(int idProfessionnel) throws SQLException {
        int nb = ProfessionnelDAO.delete(idProfessionnel);
        System.out.println("Un Professionnel a été supprimée: " + nb);
        test21_SelectMultiple();
    }
}
