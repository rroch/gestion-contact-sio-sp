package test.dao;

import java.sql.SQLException;
import java.util.List;
import modele.dao.EnseignantDAO;
import modele.dao.Jdbc;
import modele.metier.Enseignant;

/**
 * Test EnseignantDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestEnseignantDAO {

    public static void main(String[] args) {
        java.sql.Connection cnx = null;
        try {
            System.out.println("\n === Test DAO Enseignant === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            test3_Insert(999, "PAUL", "Simon");
            System.out.println("Test 3 effectué : insertion\n");
            test4_Update(999, "POL", "Simar");
            System.out.println("Test 4 effectué : mise à jour\n");
            test5_Delete(999);
            System.out.println("Test 5 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvée
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV"); 
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche un Enseignant d'après son identifiant
     *
     * @param idEnseignant 
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvée
     */
    public static void test1_SelectUnique(int idEnseignant) throws SQLException {
        Enseignant cetEnseignant = EnseignantDAO.selectOne(idEnseignant);
        if (cetEnseignant != null) {
            System.out.println("Enseignant d'identifiant : " + idEnseignant + " : " + cetEnseignant.toString());
        } else {
            System.out.println("L'enseignant d'identifiant : " + idEnseignant + " n'existe pas ");
        }

    }

    /**
     * Affiche tous les Enseignant
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvée
     */
    public static void test2_SelectMultiple() throws SQLException {
        List<Enseignant> desEnseignants = EnseignantDAO.selectAll();
        System.out.println("Les enseignant lus : " + desEnseignants.toString());
    }

    /**
     * Ajoute un enseignant
     *
     * @param idEnseignant
     * @param nomEnsei
     * @param prenomEnsei
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvée
     */
    public static void test3_Insert(int idEnseignant, String nomEnsei, String prenomEnsei) throws SQLException {
        Enseignant unEnseignant = new Enseignant(idEnseignant, "CONTANT", "Nelly");
        int nb = EnseignantDAO.insert(idEnseignant, nomEnsei, prenomEnsei);
        System.out.println("Un nouvelle Enseignant a été inséré: " + nb);
        test2_SelectMultiple();
    }

    /**
     * Modifie un enseignant
     *
     * @param idEnseignant
     * @param nomEnsei
     * @param prenomEnsei
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvée
     */
    public static void test4_Update(int idEnseignant, String nomEnsei, String prenomEnsei) throws SQLException {
        int nb = EnseignantDAO.update(idEnseignant, nomEnsei, prenomEnsei);
        System.out.println("Le enseignant " + idEnseignant + " a été mis à jour: " + nb);
        test1_SelectUnique(idEnseignant);
    }

    /**
     * Supprime un enseignant
     *
     * @param idEnseignant
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvée
     */
    public static void test5_Delete(int idEnseignant) throws SQLException {
        int nb = EnseignantDAO.delete(idEnseignant);
        System.out.println("Un enseignant a été supprimée: " + nb);
        test2_SelectMultiple();
    }
}
