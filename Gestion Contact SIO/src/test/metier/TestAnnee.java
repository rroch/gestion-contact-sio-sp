package test.metier;

import modele.metier.Annee;

/**
 * Test Métier Année
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestAnnee {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Année === \n");
        System.out.println("Test d'instanciation et d'état");
        Annee ann = new Annee(2019);
        System.out.println("Etat de l'année : " + ann.toString());
    }
}
