package test.metier;

import modele.metier.Eleve;

/**
 * Test Métier Élève
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestEleve {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Élève === \n");
        System.out.println("Test d'instanciation et d'état");
        Eleve ele = new Eleve(1, "Baudry", "Clément");
        System.out.println("Etat de l'élève : " + ele.toString());
    }
}
