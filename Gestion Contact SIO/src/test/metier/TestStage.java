package test.metier;

import java.util.ArrayList;
import modele.metier.Eleve;
import modele.metier.Entreprise;
import modele.metier.Professionnel;
import modele.metier.Stage;
import modele.metier.Technologie;
import modele.metier.Visite;

/**
 * Test Métier Stage
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestStage {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Stage === \n");
        System.out.println("Test d'instanciation et d'état");
        Eleve el = new Eleve(1, "Baudry", "Clément");
        Technologie tech = new Technologie(1, "Java");
        Entreprise entr = new Entreprise(1, "Ardelice", "07-83-12-18-42", "7 rue de la boulangerie", 44120, "Vertou");
        Professionnel pro = new Professionnel(1, "Blanchard", "Michel", "SLAM", "email@nom.com", "02-51-51-78-85", entr);
        ArrayList<Visite> lesVisites = new ArrayList<>();
        lesVisites.add(new Visite(1, false, true, true));
        Stage sta = new Stage(1, 2018, 7, "Java exo", el, entr, pro, tech, lesVisites);
        System.out.println("Etat du stage : " + sta.toString());
    }
}
