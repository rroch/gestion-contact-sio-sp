package test.metier;

import modele.metier.Enseignant;
import modele.metier.Visite;

/**
 * Test Métier Enseignant
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestEnseignant {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Enseignant === \n");
        System.out.println("Test d'instanciation et d'état");
        Enseignant ens = new Enseignant(1, "Contant", "Nelly");
        ens.ajouterVisite(new Visite(1, true, true, true));
        ens.ajouterVisite(new Visite(2, true, true, true));
        System.out.println("Etat de l'enseignant : " + ens.toString());
    }
}
