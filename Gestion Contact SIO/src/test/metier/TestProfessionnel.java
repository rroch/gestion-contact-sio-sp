package test.metier;

import modele.metier.Entreprise;
import modele.metier.Professionnel;

/**
 * Test Métier Professionnel
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestProfessionnel {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Professionnel === \n");
        System.out.println("Test d'instanciation et d'état");
        Entreprise entr = new Entreprise(1, "Ardelice", "07-83-12-18-42", "7 rue de la boulangerie", 44120, "Vertou");
        Professionnel pro = new Professionnel(1, "Blanchard", "Michel", "SLAM", "email@nom.com", "02-51-51-78-85", entr);
        System.out.println("Etat du professionnel : " + pro.toString());
    }
}
