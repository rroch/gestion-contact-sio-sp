package test.metier;

import modele.metier.Visite;

/**
 * Test Métier Visite
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestVisite {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Visite === \n");
        System.out.println("Test d'instanciation et d'état");
        Visite vis = new Visite(1, false, true, true);
        System.out.println("Etat de la visite : " + vis.toString());
    }
}
