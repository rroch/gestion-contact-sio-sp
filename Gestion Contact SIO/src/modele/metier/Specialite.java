package modele.metier;

/**
 * Classe Métier Spécialité
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class Specialite {
    
    private int id;
    private String libelle;
    private Eleve eleve;
    private Annee annee;

    /**
     * Constructeur Specialite (avec paramètres)
     * @param unId Identifiant de la spécialité
     * @param unLibelle Libellé de la spécialité
     * @param unEleve Élève de la spécialité
     * @param uneAnnee Année de la spécialité
     */
    public Specialite(int unId, String unLibelle, Annee uneAnnee, Eleve unEleve) {
        this.id = unId;
        this.libelle = unLibelle;
        this.annee = uneAnnee;
        this.eleve = unEleve;
    }

    /**
     * Constructeur Specialite (sans paramètre)
     */
    public Specialite() {
    }

    //toString
    @Override
    public String toString() {
        return "Id Spécialité: " + this.id + ", Libelle: " + this.libelle+", Libelle: "+this.eleve+", Année: "+this.annee;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Eleve getEleve() {
        return eleve;
    }

    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }
}
