package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Entreprise;

/**
 * Classe DAO Entreprise
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class EntrepriseDAO {

    /**
     * selectOne : lire un enregistrement dans la table ENTREPRISE
     *
     * @param idEntreprise
     * @return une instance de la classe métier Entreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Entreprise selectOne(int idEntreprise) throws SQLException {
        Entreprise uneEntreprise = new Entreprise();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM ENTREPRISES WHERE idEntreprise= ?");
        pstmt.setInt(1, idEntreprise);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            uneEntreprise = EntrepriseDAO.entrepriseFromResultSet(rs);
        }
        return uneEntreprise;
    }

    /**
     * selectAll : lire tous les enregistrements de la table ENTREPRISE
     *
     * @return une collection d'instances de la classe Entreprise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Entreprise> selectAll() throws SQLException {
        ArrayList<Entreprise> lesEntreprises = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENTREPRISES";
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Entreprise uneEntreprise = EntrepriseDAO.entrepriseFromResultSet(rs);
            lesEntreprises.add(uneEntreprise);
        }
        return lesEntreprises;
    }

    /**
     * Extrait un enregistrement du "ResultSet" issu de la table ENTREPRISE
     *
     * @param rs : ResultSet lu dans la table ENTREPRISE
     * @return instance de Entreprise, initialisée d'après le premier
     * enregistrement du ResultSet
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    private static Entreprise entrepriseFromResultSet(ResultSet rs) throws SQLException {
        Entreprise uneEntreprise = new Entreprise();
        uneEntreprise.setId(rs.getInt("idEntreprise"));
        uneEntreprise.setNom(rs.getString("nomEntreprise"));
        uneEntreprise.setTel(rs.getString("numTelEntreprise"));
        uneEntreprise.setAdr(rs.getString("adrEntreprise"));
        uneEntreprise.setCp(rs.getInt("cpEntreprise"));
        uneEntreprise.setVille(rs.getString("villeEntreprise"));

        return uneEntreprise;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table ENTREPRISE
     *
     * @param idEntreprise Identifiant de l'entreprise
     * @param nomEntreprise Nom de l'entreprise
     * @param adrEntreprise Adresse de l'entreprise
     * @param CPEntreprise Code Postal de l'entreprise
     * @param villeEntreprise Ville de l'entreprise
     * @param numTelEntreprise Numéro de téléphone de l'entreprise
     *
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int idEntreprise, String nomEntreprise, String numTelEntreprise, String adrEntreprise, int CPEntreprise, String villeEntreprise) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO ENTREPRISES (IDENTREPRISE, NOMENTREPRISE, NUMTELENTREPRISE, ADRENTREPRISE, CPENTREPRISE, VILLEENTREPRISE) VALUES(?,?,?,?,?,?)");
        pstmt.setInt(1, idEntreprise);
        pstmt.setString(2, nomEntreprise);
        pstmt.setString(3, numTelEntreprise);
        pstmt.setString(4, adrEntreprise);
        pstmt.setInt(5, CPEntreprise);
        pstmt.setString(6, villeEntreprise);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table ENTREPRISE
     *
     * @param idEntreprise Identifiant de l'entreprise
     * @param nomEntreprise Nom de l'entreprise
     * @param adrEntreprise Adresse de l'entreprise
     * @param CPEntreprise Code Postal de l'entreprise
     * @param villeEntreprise Ville de l'entreprise
     * @param numTelEntreprise Numéro de téléphone de l'entreprise
     *
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idEntreprise, String nomEntreprise, String numTelEntreprise, String adrEntreprise, int CPEntreprise, String villeEntreprise) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE ENTREPRISES SET NOMENTREPRISE = ?,NUMTELENTREPRISE = ?, ADRENTREPRISE = ?, CPENTREPRISE = ?, VILLEENTREPRISE = ? WHERE IDENTREPRISE = ? ");
        pstmt.setString(1, nomEntreprise);
        pstmt.setString(2, numTelEntreprise);
        pstmt.setString(3, adrEntreprise);
        pstmt.setInt(4, CPEntreprise);
        pstmt.setString(5, villeEntreprise);
        pstmt.setInt(6, idEntreprise);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table ENTREPRISE
     *
     * @param idEntreprise : identifiant conceptuel de l'Entreprise à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idEntreprise) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM ENTREPRISES WHERE IDENTREPRISE = ?");
        pstmt.setInt(1, idEntreprise);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
