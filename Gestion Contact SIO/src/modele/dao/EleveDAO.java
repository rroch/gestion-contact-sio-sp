package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Annee;
import modele.metier.Eleve;

/**
 * Classe DAO Élève
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class EleveDAO {

    /**
     * selectOne : lire un enregistrement dans la table ELEVE
     * 
     * @param idEleve Identifiant de l'élève
     * @return une instance de la classe métier Élève
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Eleve selectOne(int idEleve) throws SQLException {
        Eleve unEleve = new Eleve();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM ELEVES WHERE idEleve= ?");
        pstmt.setInt(1, idEleve);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            unEleve = EleveDAO.eleveFromResultSet(rs);
        }
        return unEleve;
    }

    /**
     * selectAll : lire tous les enregistrements de la table ELEVE
     *
     * @return une collection d'instances de la classe Élève
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Eleve> selectAll() throws SQLException {
        ArrayList<Eleve> lesEleves = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM ELEVES");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Eleve unEleve = EleveDAO.eleveFromResultSet(rs);
            lesEleves.add(unEleve);
        }
        return lesEleves;
    }

    /**
     * Extrait un enregistrement du "ResultSet" issu de la table ELEVE
     *
     * @param rs : ResultSet lu dans la table ELEVE
     * @return instance de Eleve, initialisée d'après le premier enregistrement
     * du ResultSet
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    private static Eleve eleveFromResultSet(ResultSet rs) throws SQLException {
        Eleve unEleve = new Eleve();
        unEleve.setId(rs.getInt("idEleve"));
        unEleve.setNom(rs.getString("nomEleve"));
        unEleve.setPrenom(rs.getString("prenomEleve"));
        return unEleve;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table ELEVE
     *
     * @param unId Identifiant d'un élève
     * @param unNom Nom d'un élève
     * @param unPrenom Prénom d'un élève
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int unId, String unNom, String unPrenom) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO ELEVES (IDELEVE, NOMELEVE, PRENOMELEVE) VALUES (?, ?, ?)");
        pstmt.setInt(1, unId);
        pstmt.setString(2, unNom);
        pstmt.setString(3, unPrenom);
        
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table ELEVE
     *
     * @param idEleve Identifiant conceptuel de l'élève à modifier
     * @param unNom Nom de l'élève
     * @param unPrenom Prénom de l'élève
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idEleve, String unNom, String unPrenom) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE ELEVES SET NOMELEVE = ?,PRENOMELEVE = ? WHERE IDELEVE = ?");
        pstmt.setString(1, unNom);
        pstmt.setString(2, unPrenom);
        pstmt.setInt(3, idEleve);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table ELEVE
     *
     * @param idEleve Identifiant conceptuel de l'élève à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idEleve) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM ELEVES WHERE IDELEVE = ?");
        pstmt.setInt(1, idEleve);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
