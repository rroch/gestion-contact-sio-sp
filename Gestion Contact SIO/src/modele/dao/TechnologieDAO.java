package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Technologie;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TechnologieDAO {
    /**
     * selectOne : lire un enregistrement dans la table TECHNOLOGIE
     *
     * @param id : identifiant de la Technologie recherchée
     * @return une instance de la classe métier Technologie
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Technologie selectOne(int id) throws SQLException {
        Technologie uneTech = new Technologie();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM TECHNOLOGIE WHERE IDTECH= ?");
        pstmt.setInt(1, id);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            uneTech = new Technologie();
            uneTech.setId(rs.getInt("IDTECH"));
            uneTech.setLibelle(rs.getString("LIBELLETECH"));
        }
        return uneTech;
    }

    /**
     * selectAll : lire tous les enregistrements de la table TECHNOLOGIE
     *
     * @return une collection d'instances de la classe Technologie
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Technologie> selectAll() throws SQLException {
        ArrayList<Technologie> lesTechs = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM TECHNOLOGIE");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Technologie uneTech = new Technologie();
            uneTech.setId(rs.getInt("IDTECH"));
            uneTech.setLibelle(rs.getString("LIBELLETECH"));
            lesTechs.add(uneTech);
        }
        return lesTechs;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table TECHNOLOGIE
     *
     * @param id Identifiant d'une technologie
     * @param libelle Libellé d'une technologie
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int id, String libelle) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO TECHNOLOGIE VALUES (?, ?)");
        pstmt.setInt(1, id);
        pstmt.setString(2, libelle);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table TECHNOLOGIE
     *
     * @param id Identifiant d'une technologie
     * @param libelle Libellé d'une technologie
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int id, String libelle) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE TECHNOLOGIE SET LIBELLETECH= ? WHERE IDTECH= ?");
        pstmt.setString(1, libelle);
        pstmt.setInt(2, id);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table TECHNOLOGIE
     *
     * @param id : Identifiant de la Technologie à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int id) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM TECHNOLOGIE WHERE IDTECH= ?");
        pstmt.setInt(1, id);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
