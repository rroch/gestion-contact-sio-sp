package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Stage;

/**
 * Classe DAO Stage
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class StageDAO {

    /**
     * selectOne : lire un enregistrement dans la table STAGE
     *
     * @param idStage Identifiant du stage
     * @return une instance de la classe métier Stage
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Stage selectOne(int idStage) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        Stage unStage = new Stage();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM STAGES WHERE idStage= ?");
        pstmt.setInt(1, idStage);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            unStage = stageFromResultSet(rs);
        }
        return unStage;
    }

    /**
     * selectAll : lire tous les enregistrements de la table STAGE
     *
     * @return une collection d'instances de la classe Stage
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Stage> selectAll() throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM STAGES");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Lire tous les enregistrements de la table STAGE qui ont le même
     * idEntreprise dans la BDD
     *
     * @param idEntreprise Identifiant de l'entreprise
     * @return Liste des stages concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Stage> selectStageByEntreprise(int idEntreprise) throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM STAGES WHERE IDENTREPRISE = ?");
        pstmt.setInt(1, idEntreprise);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Lire tous les enregistrements de la table STAGE qui ont le même idEleve
     * dans la BDD
     *
     * @param idEleve Identifiant de l'élève stagiaire
     * @return Liste des stages concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Stage> selectStageByEleve(int idEleve) throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM STAGES WHERE IDELEVE = ?");
        pstmt.setInt(1, idEleve);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Lire tous les enregistrements de la table STAGE
     *
     *
     * @param anneeDebut
     * @param anneeArrive
     * @return Liste des stages concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Stage> selectStageByAnnee(int anneeDebut, int anneeArrive) throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(ST.idStage),ST.sujetStage,ST.idProfessionnel,ST.idTechno,ST.periodeStage,ST.IDELEVE,ST.IDELEVE,ST.IDENTREPRISE, ST.ANNEESTAGE FROM STAGES ST INNER JOIN ENTREPRISES EN ON ST.idEntreprise = EN.idEntreprise INNER JOIN ELEVES E ON ST.idEleve = E.idEleve INNER JOIN PROFESSIONNELS P ON ST.idProfessionnel = P.idProf WHERE ST.ANNEESTAGE BETWEEN ? AND ?");
        pstmt.setInt(1, anneeDebut);
        pstmt.setInt(2, anneeArrive);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Lire tous les enregistrements de la table STAGE en fonction de la Specialite
     *
     *
     * @param anneeDebut
     * @param anneeArrive
     * @param spe
     * @return Liste des stages concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Stage> selectStageByAnneeAndSpe(int anneeDebut, int anneeArrive, String spe) throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(ST.idStage),ST.sujetStage,ST.idProfessionnel,ST.idTechno,ST.periodeStage,ST.IDELEVE,ST.IDELEVE,ST.IDENTREPRISE, ST.ANNEESTAGE FROM STAGES ST INNER JOIN ENTREPRISES EN ON ST.idEntreprise = EN.idEntreprise INNER JOIN ELEVES E ON ST.idEleve = E.idEleve INNER JOIN PROFESSIONNELS P ON ST.idProfessionnel = P.idProf INNER JOIN SPECIALITE SPE ON E.idEleve = SPE.idEleve WHERE ST.ANNEESTAGE BETWEEN ? AND ? AND SPE.libelleSpecialite LIKE ?");
        pstmt.setInt(1, anneeDebut);
        pstmt.setInt(2, anneeArrive);
        pstmt.setString(3, spe);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Lire tous les enregistrements de la table STAGE en fonction d'un
     * professionnel
     *
     * @param pro
     * @return Liste des stages concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Stage> selectStagiaireByPro(String pro) throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(ST.idStage),ST.sujetStage,ST.idProfessionnel,ST.idTechno,ST.periodeStage,ST.IDELEVE,ST.IDELEVE,ST.IDENTREPRISE, ST.ANNEESTAGE  FROM STAGES ST INNER JOIN ENTREPRISES EN ON ST.idEntreprise = EN.idEntreprise INNER JOIN ELEVES E ON ST.idEleve = E.idEleve INNER JOIN PROFESSIONNELS P ON ST.idProfessionnel = P.idProf WHERE P.nomProf = ?");
        pstmt.setString(1, pro);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Lire tous les enregistrements de la table STAGE en fonction d'une
     * entreprise
     *
     * @param entre
     * @return Liste des stages concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Stage> selectStagiaireByEntre(String entre) throws SQLException {
        ArrayList<Stage> lesStages = new ArrayList<>();
        Stage unStage;
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(ST.idStage),ST.sujetStage,ST.idProfessionnel,ST.idTechno,ST.periodeStage,ST.IDELEVE,ST.IDELEVE,ST.IDENTREPRISE, ST.ANNEESTAGE FROM STAGES ST INNER JOIN ENTREPRISES EN ON ST.idEntreprise = EN.idEntreprise INNER JOIN ELEVES E ON ST.idEleve = E.idEleve INNER JOIN PROFESSIONNELS P ON ST.idProfessionnel = P.idProf WHERE EN.nomEntreprise = ?");
        pstmt.setString(1, entre);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            unStage = stageFromResultSet(rs);
            lesStages.add(unStage);
        }
        return lesStages;
    }

    /**
     * Extrait un enregistrement du "ResultSet" issu de la table STAGE
     *
     * @param rs : ResultSet lu dans la table STAGE
     * @return instance de STAGE, initialisée d'après le premier enregistrement
     * du ResultSet
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    private static Stage stageFromResultSet(ResultSet rs) throws SQLException {
        Stage unStage = new Stage();
        unStage.setId(rs.getInt("idStage"));
        unStage.setAnneeStage(rs.getInt("anneeStage"));
        unStage.setPeriodeStage(rs.getInt("periodeStage"));
        unStage.setUnSujetStage(rs.getString("sujetStage"));
        unStage.setUnEleve(EleveDAO.selectOne(rs.getInt("idEleve")));
        unStage.setUnProfessionnel(ProfessionnelDAO.selectOne(rs.getInt("idProfessionnel")));
        unStage.setUneEntreprise(EntrepriseDAO.selectOne(rs.getInt("idEntreprise")));
        unStage.setUneTechnologie(TechnologieDAO.selectOne(rs.getInt("idTechno")));
        unStage.setLesVisites(VisiteDAO.selectAllByStage(rs.getInt("idStage")));
        return unStage;
    }

    /**
     * Insert : ajouter un enregistrement dans la table STAGE
     *
     * @param idStage Identifiant d'une stage
     * @param anneeStage Année d'un stage
     * @param periodeStage Période d'un stage
     * @param sujetStage Sujet du stage
     * @param idEntreprise Identifiant de l'entreprise d'un stage
     * @param idEleve Identifiant de l'élève stagiaire
     * @param idProfessionnel Identifiant du tuteur de stage
     * @param idTechno Technologie étudiée
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int idStage, int anneeStage, int periodeStage, String sujetStage, int idEntreprise, int idEleve, int idProfessionnel, int idTechno) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO STAGES (IDSTAGE, ANNEESTAGE, PERIODESTAGE, SUJETSTAGE ,IDENTREPRISE, IDELEVE, IDPROFESSIONNEL,IDTECHNO) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        pstmt.setInt(1, idStage);
        pstmt.setInt(2, anneeStage);
        pstmt.setInt(3, periodeStage);
        pstmt.setString(4, sujetStage);
        pstmt.setInt(5, idEntreprise);
        pstmt.setInt(6, idEleve);
        pstmt.setInt(7, idProfessionnel);
        pstmt.setInt(8, idTechno);

        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table STAGE
     *
     * @param idStage Identifiant du stage à modifier
     * @param anneeStage Année du Stage
     * @param periodeStage Période du Stage
     * @param sujetStage Sujet du Stage
     * @param idEntreprise Identifiant de l'entreprise du stage
     * @param idEleve Identifiant du stagiaire
     * @param idProfessionnel Identifiant du maitre de stage
     * @param idTechno identifiant de la technologie étudiée
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idStage, int anneeStage, int periodeStage, String sujetStage, int idEntreprise, int idEleve, int idProfessionnel, int idTechno) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE STAGES SET ANNEESTAGE = ?, PERIODESTAGE = ?, SUJETSTAGE = ?, IDENTREPRISE = ?, IDELEVE = ?,IDPROFESSIONNEL = ?, IDTECHNO = ? WHERE IDSTAGE = ?");
        pstmt.setInt(1, anneeStage);
        pstmt.setInt(2, periodeStage);
        pstmt.setString(3, sujetStage);
        pstmt.setInt(4, idEntreprise);
        pstmt.setInt(5, idEleve);
        pstmt.setInt(6, idProfessionnel);
        pstmt.setInt(7, idTechno);
         pstmt.setInt(8, idStage);

        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table STAGE
     *
     * @param idStage dentifiant du stage à supprimer
     * @return 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idStage) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM STAGES WHERE IDSTAGE = ?");
        pstmt.setInt(1, idStage);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
