package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Specialite;

/**
 * Classe DAO Spécialité
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class SpecialiteDAO {

    /**
     * selectOne : lire un enregistrement dans la table SPECIALITE
     *
     * @param idSpecialite
     * @return une instance de la classe métier Spécialité
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Specialite selectOne(int idSpecialite) throws SQLException {
        Specialite uneSpecialite = new Specialite();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM SPECIALITE WHERE idSpecialite= ?");
        pstmt.setInt(1, idSpecialite);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            uneSpecialite = specialiteFromResultSet(rs);
        }
        return uneSpecialite;
    }

    /**
     * selectAll : lire tous les enregistrements de la table SPECIALITE
     *
     * @return une collection d'instances de la classe Spécialité
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Specialite> selectAll() throws SQLException {
        ArrayList<Specialite> lesSpecialites = new ArrayList<>();
        Specialite uneSpe;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM SPECIALITE");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            uneSpe = specialiteFromResultSet(rs);
            lesSpecialites.add(uneSpe);
        }
        return lesSpecialites;
    }

    /**
     * Extrait un enregistrement du "ResultSet" issu de la table SPECIALITE
     *
     * @param rs : ResultSet lu dans la table SPECIALITE
     * @return instance de Specialite, initialisée d'après le premier enregistrement
     * du ResultSet
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    private static Specialite specialiteFromResultSet(ResultSet rs) throws SQLException {
        Specialite uneSpecialite = new Specialite();
        uneSpecialite.setId(rs.getInt("idSpecialite"));
        uneSpecialite.setLibelle(rs.getString("libelleSpecialite"));
        uneSpecialite.setAnnee(AnneeDAO.selectOne(rs.getInt("dateAnnee")));
        uneSpecialite.setEleve(EleveDAO.selectOne(rs.getInt("idEleve")));
        return uneSpecialite;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table Specialiter
     *
     * @param idSpecialite Identifiant de la spécialité à ajouter
     * @param libelleSpecialite Libellé de la spécialité
     * @param idEleve Identifiant de l'élève
     * @param dateAnnee Date de la spécialité
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int idSpecialite, String libelleSpecialite, int idEleve, int dateAnnee) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO SPECIALITE (IDSPECIALITE, LIBELLESPECIALITE, DATEANNEE, IDELEVE) VALUES (?, ?, ?, ?)");
        pstmt.setInt(1, idSpecialite);
        pstmt.setString(2, libelleSpecialite);
        pstmt.setInt(3, dateAnnee);
        pstmt.setInt(4, idEleve);

        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table SPECIALITE
     *
     * @param idSpecialite Identifiant de la spécialité
     * @param libelleSpecialite Libellé de la spécialité
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idSpecialite, String libelleSpecialite) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE SPECIALITE SET LIBELLESPECIALITE = ? WHERE IDSPECIALITE = ?");
        pstmt.setString(1, libelleSpecialite);
        pstmt.setInt(2, idSpecialite);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table SPECIALITE
     *
     * @param idSpecialite : identifiant conceptuel de la specialite à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idSpecialite) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM SPECIALITE WHERE IDSPECIALITE = ?");
        pstmt.setInt(1, idSpecialite);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
