package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Professionnel;

/**
 * Classe DAO Professionnel
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class ProfessionnelDAO {

    /**
     * selectOne : lire un enregistrement dans la table PROFESSIONNEL
     *
     * @param idProfessionnel identifiant
     * @return une instance de la classe métier Professionnel
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Professionnel selectOne(int idProfessionnel) throws SQLException {
        Professionnel unProfessionnel = new Professionnel();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM PROFESSIONNELS WHERE IDPROF= ?");
        pstmt.setInt(1, idProfessionnel);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            unProfessionnel = professionnelFromResultSet(rs);
        }
        return unProfessionnel;
    }

    /**
     * selectAll : lire tous les enregistrements de la table PROFESSIONNEL
     *
     * @return une collection d'instances de la classe Professionnel
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Professionnel> selectAll() throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM PROFESSIONNELS");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Lire tous les enregistrements de la table PROFESSIONNEL qui ont donner
     * leurs accord pour participer au jury entre deux années données
     *
     * @param anneeDebut
     * @param anneeArrive
     * @return Liste des professionnels concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Professionnel> selectProfByAccordAnnee(int anneeDebut, int anneeArrive) throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(P.IDPROF), P.NOMPROF,P.PRENOMPROF,P.SPECIALITEPROF, P.NUMEROTELPROF,P.MAILPROF,P.IDENTREPRISE,S.ANNEESTAGE FROM PROFESSIONNELS P INNER JOIN STAGES S ON P.IDPROF = S.IDPROFESSIONNEL INNER JOIN VISITE V ON V.IDSTAGE = S.IDSTAGE WHERE V.ACCORDJURY=1 AND S.ANNEESTAGE BETWEEN ? AND ?");
        pstmt.setInt(1, anneeDebut);
        pstmt.setInt(2, anneeArrive);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Lire tous les enregistrements de la table PROFESSIONNEL qui ont donner
     * leurs accord pour participer au jury entre deux années données
     *
     * @param anneeDebut
     * @param anneeArrive
     * @param uneSpe
     * @return Liste des professionnels concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Professionnel> selectProfByAccordAnneeSpe(int anneeDebut, int anneeArrive, String uneSpe) throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(P.IDPROF), P.NOMPROF,P.PRENOMPROF,P.SPECIALITEPROF, P.NUMEROTELPROF,P.MAILPROF,P.IDENTREPRISE,S.ANNEESTAGE FROM PROFESSIONNELS P INNER JOIN STAGES S ON P.IDPROF = S.IDPROFESSIONNEL INNER JOIN VISITE V ON V.IDSTAGE = S.IDSTAGE WHERE V.ACCORDJURY=1 AND S.ANNEESTAGE BETWEEN ? AND ? AND P.specialiteProf = ?");
        pstmt.setInt(1, anneeDebut);
        pstmt.setInt(2, anneeArrive);
        pstmt.setString(3, uneSpe);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Lire tous les enregistrements de la table PROFESSIONNEL qui ont donner
     * leurs accord pour prendre un stagiaire entre deux années données
     *
     * @param anneeDebut
     * @param anneeArrive
     * @return Liste des professionnels concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Professionnel> selectProfByAccordStageAnnee(int anneeDebut, int anneeArrive) throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(PRO.IDPROF), PRO.NOMPROF, PRO.PRENOMPROF, PRO.SPECIALITEPROF, PRO.MAILPROF, PRO.NUMEROTELPROF, EN.NOMENTREPRISE, EN.NUMTELENTREPRISE, EN.ADRENTREPRISE, EN.CPENTREPRISE, EN.VILLEENTREPRISE FROM PROFESSIONNELS PRO INNER JOIN STAGES S ON PRO.idProf = S.idProfessionnel INNER JOIN ENTREPRISES EN ON S.IDENTREPRISE = EN.IDENTREPRISE INNER JOIN ELEVES E ON S.IDELEVE = E.IDELEVE INNER JOIN VISITE V ON S.IDSTAGE = V.idStage INNER JOIN SPECIALITE SPE ON E.IDELEVE = SPE.IDELEVE WHERE S.ANNEESTAGE BETWEEN ? AND ?;\n" + "");
        pstmt.setInt(1, anneeDebut);
        pstmt.setInt(2, anneeArrive);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Lire tous les enregistrements de la table PROFESSIONNEL qui ont donner
     * leurs accord pour prendre un stagiaire entre deux années données pour une
     * période donnée
     *
     * @param unePerio
     * @param anneeDebut
     * @param anneeArrive
     * @return Liste des professionnels concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Professionnel> selectProfByAccordStageAnneePerio(String unePerio, int anneeDebut, int anneeArrive) throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();

        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(PRO.IDPROF), PRO.NOMPROF, PRO.PRENOMPROF, PRO.SPECIALITEPROF, PRO.MAILPROF, PRO.NUMEROTELPROF, EN.NOMENTREPRISE, EN.NUMTELENTREPRISE, EN.ADRENTREPRISE, EN.CPENTREPRISE, EN.VILLEENTREPRISE FROM PROFESSIONNELS PRO INNER JOIN STAGES S ON PRO.idProf = S.idProfessionnel INNER JOIN ENTREPRISES EN ON S.IDENTREPRISE = EN.IDENTREPRISE INNER JOIN ELEVES E ON S.IDELEVE = E.IDELEVE INNER JOIN VISITE V ON S.IDSTAGE = V.idStage INNER JOIN SPECIALITE SPE ON E.IDELEVE = SPE.IDELEVE WHERE V." + unePerio + "=1 AND S.ANNEESTAGE BETWEEN ? AND ?;\n" + "");
        pstmt.setInt(1, anneeDebut);
        pstmt.setInt(2, anneeArrive);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Lire tous les enregistrements de la table PROFESSIONNEL qui ont donner
     * leurs accord pour prendre un stagiaire entre deux années données pour une
     * spécialité donnée
     *
     * @param uneSpe
     * @param anneeDebut
     * @param anneeArrive
     * @return Liste des professionnels concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Professionnel> selectProfByAccordStageAnneeSpe(String uneSpe, int anneeDebut, int anneeArrive) throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(PRO.IDPROF), PRO.NOMPROF, PRO.PRENOMPROF, PRO.SPECIALITEPROF, PRO.MAILPROF, PRO.NUMEROTELPROF, EN.NOMENTREPRISE, EN.NUMTELENTREPRISE, EN.ADRENTREPRISE, EN.CPENTREPRISE, EN.VILLEENTREPRISE FROM PROFESSIONNELS PRO INNER JOIN STAGES S ON PRO.idProf = S.idProfessionnel INNER JOIN ENTREPRISES EN ON S.IDENTREPRISE = EN.IDENTREPRISE INNER JOIN ELEVES E ON S.IDELEVE = E.IDELEVE INNER JOIN VISITE V ON S.IDSTAGE = V.idStage INNER JOIN SPECIALITE SPE ON E.IDELEVE = SPE.IDELEVE WHERE PRO.specialiteProf= ? AND S.ANNEESTAGE BETWEEN ? AND ?;\n" + "");
        pstmt.setString(1, uneSpe);
        pstmt.setInt(2, anneeDebut);
        pstmt.setInt(3, anneeArrive);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Lire tous les enregistrements de la table PROFESSIONNEL qui ont donner
     * leurs accord pour prendre un stagiaire
     *
     * @param anneeDebut
     * @param anneeArrive
     * @param unePerio
     * @param uneSpe
     * @return Liste des professionnels concernés
     * @throws java.sql.SQLException Retourne une Exception SQL si une erreur
     * est trouvé
     */
    public static ArrayList<Professionnel> selectProfByAccordStageAll(String uneSpe, int anneeDebut, int anneeArrive, String unePerio) throws SQLException {
        ArrayList<Professionnel> lesProfessionnels = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT DISTINCT(PRO.IDPROF), PRO.NOMPROF, PRO.PRENOMPROF, PRO.SPECIALITEPROF, PRO.MAILPROF, PRO.NUMEROTELPROF, EN.NOMENTREPRISE, EN.NUMTELENTREPRISE, EN.ADRENTREPRISE, EN.CPENTREPRISE, EN.VILLEENTREPRISE FROM PROFESSIONNELS PRO INNER JOIN STAGES S ON PRO.idProf = S.idProfessionnel INNER JOIN ENTREPRISES EN ON S.IDENTREPRISE = EN.IDENTREPRISE INNER JOIN ELEVES E ON S.IDELEVE = E.IDELEVE INNER JOIN VISITE V ON S.IDSTAGE = V.idStage INNER JOIN SPECIALITE SPE ON E.IDELEVE = SPE.IDELEVE WHERE V." + unePerio + "=1 AND PRO.specialiteProf= ? AND S.ANNEESTAGE BETWEEN ? AND ?;");
        pstmt.setString(1, uneSpe);
        pstmt.setInt(2, anneeDebut);
        pstmt.setInt(3, anneeArrive);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Professionnel unProfessionnel = professionnelFromResultSet(rs);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    /**
     * Extrait un enregistrement du "ResultSet" issu de la table PROFESSIONNEL
     *
     * @param rs : ResultSet lu dans la table PROFESSIONNEL
     * @return instance de PROFESSIONNEL, initialisée d'après le premier
     * enregistrement du ResultSet
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    private static Professionnel professionnelFromResultSet(ResultSet rs) throws SQLException {
        Professionnel unProfessionnel = new Professionnel();
        unProfessionnel.setId(rs.getInt("idProf"));
        unProfessionnel.setNom(rs.getString("nomProf"));
        unProfessionnel.setPrenom(rs.getString("prenomProf"));
        unProfessionnel.setSpe(rs.getString("specialiteProf"));
        unProfessionnel.setMail(rs.getString("mailProf"));
        unProfessionnel.setTel(rs.getString("numeroTelProf"));
        unProfessionnel.setEntreprise(EntrepriseDAO.selectOne(rs.getInt("idProf")));

        return unProfessionnel;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table PROFESSIONNEL
     *
     * @param idProfessionnel Identifiant du professionnel à ajouter
     * @param nomProf Nom du professionnel
     * @param prenomProf Prénom du professionnel
     * @param specialiteProf Specialité du professionnel
     * @param mailProf Email du professionnel
     * @param numeroTelProf Numéro de téléphone du professionnel
     * @param idEntreprise Identifiant de l'entreprise
     *
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int idProfessionnel, String nomProf, String prenomProf, String specialiteProf, String mailProf, String numeroTelProf, int idEntreprise) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO PROFESSIONNELS (IDPROF, NOMPROF, PRENOMPROF, SPECIALITEPROF, MAILPROF, NUMEROTELPROF,IDENTREPRISE) VALUES (?, ?, ?, ?, ?, ?, ?)");
        pstmt.setInt(1, idProfessionnel);
        pstmt.setString(2, nomProf);
        pstmt.setString(3, prenomProf);
        pstmt.setString(4, specialiteProf);
        pstmt.setString(5, mailProf);
        pstmt.setString(6, numeroTelProf);
        pstmt.setInt(7, idEntreprise);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table PROFESSIONNEL
     *
     * @param idProfessionnel Identifiant du professionnel
     * @param nomProf Nom du professionnel
     * @param prenomProf Adresse du professionnel
     * @param specialiteProf Spécialité du professionnel
     * @param mailProf Ville du professionnel
     * @param numeroTelProf Numéro de téléphone du professionnel
     *
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idProfessionnel, String nomProf, String prenomProf, String specialiteProf, String mailProf, String numeroTelProf) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "UPDATE PROFESSIONNELS SET NOMPROF = ? , PRENOMPROF = ?, SPECIALITEPROF = ?, MAILPROF = ?, NUMEROTELPROF = ? WHERE IDPROF = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setString(1, nomProf);
        pstmt.setString(2, prenomProf);
        pstmt.setString(3, specialiteProf);
        pstmt.setString(4, mailProf);
        pstmt.setString(5, numeroTelProf);
        pstmt.setInt(6, idProfessionnel);
        nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table PROFESSIONNEL
     *
     * @param idProfessionnel : identifiant du professionnel à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idProfessionnel) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM PROFESSIONNELS WHERE IDPROF = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idProfessionnel);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
