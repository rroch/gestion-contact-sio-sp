package csv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import modele.dao.EleveDAO;
import modele.dao.EntrepriseDAO;

/**
 * Classe CSV Import
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class CSVImport {

    public static boolean importCSV() throws IOException, SQLException {
        JFileChooser choix = new JFileChooser();
        int retour = choix.showOpenDialog(null);
        if (retour == JFileChooser.APPROVE_OPTION) {
            final List<String[]> data = CsvFileHelper.readCsvFile(choix.getSelectedFile(), ';');
            String etape = "";
            for (String[] oneData : data) {
                // Pour chaque champ, on affche la donnée (test visuel)
                System.out.print(oneData[0]);
                
            }
            switch (etape) {
                case "annee":
                    break;
                case "eleves":
                    break;
                case "enseignants":
                    break;
                case "entreprises":
                    break;
                case "professionnels":
                    break;
                case "specialite":
                    break;
                case "stages":
                    break;
                case "technologie":
                    break;
                case "visite":
                    break;
            }
            System.out.println("");
            return false;
        } else {
            JOptionPane.showMessageDialog(null, "Aucun Fichier n'a été choisi");
            return true;
        }

    }

}
