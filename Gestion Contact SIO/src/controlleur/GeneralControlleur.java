package controlleur;

import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 * Controlleur General
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class GeneralControlleur {

    private MainControlleur ctrlMain;
    private StagiairesEncadresControlleur ctrlStagiairesEncadres;
    private ListeStageControlleur ctrlListeStage;
    private ContactAccordJuryControlleur ctrlContactAccordJury;
    private ContactAccordStagiairesControlleur ctrlContactAccordStagiaires;

    /**
     * Affiche la Vue Main et son controleur
     */
    public void afficherMain() {
        this.ctrlMain.getVue().setVisible(true);
        this.ctrlStagiairesEncadres.getVue().setVisible(false);
        this.ctrlListeStage.getVue().setVisible(false);
        this.ctrlContactAccordJury.getVue().setVisible(false);
        this.ctrlContactAccordStagiaires.getVue().setVisible(false);
    }

    /**
     * Affiche la Vue Contact Accord Jury et son controleur
     */
    public void afficherLesContactAccordJury() {
        this.ctrlMain.getVue().setVisible(false);
        this.ctrlStagiairesEncadres.getVue().setVisible(false);
        this.ctrlListeStage.getVue().setVisible(false);
        this.ctrlContactAccordJury.getVue().setVisible(true);
        this.ctrlContactAccordStagiaires.getVue().setVisible(false);
    }

    public void afficherLesContactAccordStagiaires() {
        this.ctrlMain.getVue().setVisible(false);
        this.ctrlStagiairesEncadres.getVue().setVisible(false);
        this.ctrlListeStage.getVue().setVisible(false);
        this.ctrlContactAccordJury.getVue().setVisible(false);
        this.ctrlContactAccordStagiaires.getVue().setVisible(true);
    }

    public void afficherLesListesStages() {
        this.ctrlMain.getVue().setVisible(false);
        this.ctrlStagiairesEncadres.getVue().setVisible(false);
        this.ctrlListeStage.getVue().setVisible(true);
        this.ctrlContactAccordJury.getVue().setVisible(false);
        this.ctrlContactAccordStagiaires.getVue().setVisible(false);
    }

    public void afficherLesStagiairesEncadres() {
        this.ctrlMain.getVue().setVisible(false);
        this.ctrlStagiairesEncadres.getVue().setVisible(true);
        this.ctrlListeStage.getVue().setVisible(false);
        this.ctrlContactAccordJury.getVue().setVisible(false);
        this.ctrlContactAccordStagiaires.getVue().setVisible(false);
    }

    public void quitterApplication() {
        // Confirmer avant de quitter
        int rep = JOptionPane.showConfirmDialog(null, "Quitter l'application\nEtes-vous sûr(e) ?", "Gestion Contact SIO", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rep == JOptionPane.YES_OPTION) {
            // mettre fin à l'application
            System.exit(0);
        }
    }

    public void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 10; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width + 1, width);
            }
            if (width > 300) {
                width = 300;
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    public MainControlleur getCtrlMain() {
        return ctrlMain;
    }

    public void setCtrlMain(MainControlleur ctrlMain) {
        this.ctrlMain = ctrlMain;
    }

    public StagiairesEncadresControlleur getCtrlStagiairesEncadres() {
        return ctrlStagiairesEncadres;
    }

    public void setCtrlStagiairesEncadres(StagiairesEncadresControlleur ctrlStagiairesEncadres) {
        this.ctrlStagiairesEncadres = ctrlStagiairesEncadres;
    }

    public ListeStageControlleur getCtrlListeStage() {
        return ctrlListeStage;
    }

    public void setCtrlListeStage(ListeStageControlleur ctrlListeStage) {
        this.ctrlListeStage = ctrlListeStage;
    }

    public ContactAccordJuryControlleur getCtrlContactAccordJury() {
        return ctrlContactAccordJury;
    }

    public void setCtrlContactAccordJury(ContactAccordJuryControlleur ctrlContactAccordJury) {
        this.ctrlContactAccordJury = ctrlContactAccordJury;
    }

    public ContactAccordStagiairesControlleur getCtrlContactAccordStagiaires() {
        return ctrlContactAccordStagiaires;
    }

    public void setCtrlContactAccordStagiaires(ContactAccordStagiairesControlleur ctrlContactAccordStagiaires) {
        this.ctrlContactAccordStagiaires = ctrlContactAccordStagiaires;
    }

}
