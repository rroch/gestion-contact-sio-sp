package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JOptionPane;
//import java.io.BufferedReader;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import csv.CSVImport;
import modele.dao.AnneeDAO;
//import modele.dao.Jdbc;
import vue.VueMain;

/**
 * Controlleur Main
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class MainControlleur implements WindowListener, ActionListener {

    // Vue
    private VueMain vue;
    private GeneralControlleur ctrlGeneral;

    public MainControlleur(VueMain vue, GeneralControlleur ctrl) throws SQLException {
        this.vue = vue;
        this.ctrlGeneral = ctrl;
        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);
        this.vue.getjButtonContactAccordJury().addActionListener(this);
        this.vue.getjButtonContactAccordStagiaire().addActionListener(this);
        this.vue.getjButtonListeStage().addActionListener(this);
        this.vue.getjButtonStagiairesEncadres().addActionListener(this);
        this.vue.getjButtonImportCSV().addActionListener(this);

        if (AnneeDAO.selectAll().isEmpty()) {
            this.vue.getjButtonContactAccordJury().setEnabled(false);
            this.vue.getjButtonContactAccordStagiaire().setEnabled(false);
            this.vue.getjButtonListeStage().setEnabled(false);
            this.vue.getjButtonStagiairesEncadres().setEnabled(false);
            JOptionPane.showMessageDialog(null, "Aucune donnée dans la base de données, pensez à importer le Script SQL et renlacer l'application");
        }
    }

    /*
    public void importCSV() throws IOException, SQLException {

        // Si utilisateur veux importer la bdd
        if (JOptionPane.showConfirmDialog(null, "Importer une nouvelle version de la BDD entrainera la perte de l'ancienne BDD continuer ?", "ATTENTION", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            //Suppression et ajout BDD Vierge
            Jdbc jdbc = Jdbc.getInstance();
            InputStream flux = new FileInputStream("src/sql/bddgestioncontact.min.sql");
            InputStreamReader lecture = new InputStreamReader(flux);
            BufferedReader buff = new BufferedReader(lecture);
            String laLigne;
            while ((laLigne = buff.readLine()) != null) {
                Statement stmt = jdbc.getConnexion().createStatement();
                stmt.executeUpdate(laLigne);

            }
            JOptionPane.showMessageDialog(null, "Base de donnée NETTOYÉE");
            Boolean enCours = true;
            int essai = 0;

            // EssaiInsert du Fichier CSV
            while (essai <= 3 && enCours == true) {
                JOptionPane.showMessageDialog(null, "Choisir Fichier contenant Liste Etudiants, nb essai restant : " + (3 - essai));
                enCours = CSVImport.importCSV();
                essai++;
            }
            //Si nb essai est dépassé
            if (essai >= 4) {
                JOptionPane.showMessageDialog(null, "Nombre d'essai dépassé, veuillez rééssayer");
            } else {
                JOptionPane.showMessageDialog(null, "BDD Importé");
            }

        }

    }
     */
    public GeneralControlleur getCtrlGeneral() {
        return ctrlGeneral;
    }

    public void setCtrlGeneral(GeneralControlleur ctrlGeneral) {
        this.ctrlGeneral = ctrlGeneral;
    }

    public VueMain getVue() {
        return vue;
    }

    public void setVue(VueMain vue) {
        this.vue = vue;
    }

    // ACCESSEURS et MUTATEURS
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        ctrlGeneral.quitterApplication();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonContactAccordJury())) {
            ctrlGeneral.afficherLesContactAccordJury();
        }
        if (e.getSource().equals(vue.getjButtonContactAccordStagiaire())) {
            ctrlGeneral.afficherLesContactAccordStagiaires();
        }
        if (e.getSource().equals(vue.getjButtonListeStage())) {
            ctrlGeneral.afficherLesListesStages();
        }
        if (e.getSource().equals(vue.getjButtonStagiairesEncadres())) {
            ctrlGeneral.afficherLesStagiairesEncadres();
        }

        /*
        if (e.getSource().equals(vue.getjButtonImportCSV())) {

            try {
                importCSV();
            } catch (IOException | SQLException ex) {
                Logger.getLogger(MainControlleur.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         */
    }

}
