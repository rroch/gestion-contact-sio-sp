package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modele.dao.AnneeDAO;
import modele.dao.StageDAO;
import modele.metier.Annee;
import modele.metier.Stage;
import vue.VueListeStage;

/**
 * Controlleur ListeStage
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class ListeStageControlleur implements WindowListener, ActionListener {

    private VueListeStage vue; // Vue
    private final GeneralControlleur generalCtrl;
    private String spe;

    public ListeStageControlleur(VueListeStage vue, GeneralControlleur ctrl) throws SQLException {
        this.vue = vue;
        this.generalCtrl = ctrl;

        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);

        // le contrôleur écoute les actions des boutons et des radio boutons
        this.vue.getjButtonRefresh().addActionListener(this);
        this.vue.getjButtonRetour().addActionListener(this);
        this.vue.getjRadioButtonTous().addActionListener(this);
        this.vue.getjRadioButtonSISR().addActionListener(this);
        this.vue.getjRadioButtonSLAM().addActionListener(this);

        // préparer l'état initial de la vue
        ArrayList<Annee> lesAnnees = AnneeDAO.selectAll();
        if (!lesAnnees.isEmpty()) {
            //Remise à zéro des JComboBox
            this.vue.getjComboBoxAnneeDebut().setModel(new javax.swing.DefaultComboBoxModel());
            this.vue.getjComboBoxAnneeFin().setModel(new javax.swing.DefaultComboBoxModel());
            //Remplissage des JComboBox avec les années
            for (Annee uneAnne : lesAnnees) {
                this.vue.getjComboBoxAnneeDebut().addItem(Integer.toString(uneAnne.getDateAnnee()));
                this.vue.getjComboBoxAnneeFin().addItem(Integer.toString(uneAnne.getDateAnnee()));
            }
            //Sélection des Années de début et de fin par défaut
            this.vue.getjComboBoxAnneeDebut().setSelectedIndex(0);
            this.vue.getjComboBoxAnneeFin().setSelectedIndex(this.vue.getjComboBoxAnneeDebut().getItemCount() - 1);
            //Appel du remplissage de la Table Stage
            afficherListeStage();
        }
    }

    /**
     * Remplissage de la table Stage
     */
    public final void afficherListeStage() {
        ArrayList<Stage> lesStages;
        JFrame frame = new JFrame("showMessageDialog");
        try {
            //Si Année début inférieur à Année fin
            if (Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()) - Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()) <= 0) {
                //Si le Radio Bouton Tous est sélectionné
                if (getVue().getjRadioButtonTous().isSelected()) {
                    lesStages = StageDAO.selectStageByAnnee(Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()));
                } else { //Si un autre radio Bouton est sélectionné
                    lesStages = StageDAO.selectStageByAnneeAndSpe(Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()), spe);
                }
                vue.getModeleTableStage().setRowCount(0);
                String[] titresColonnes = {"Entreprise", "Adresse Entreprise", "Téléphone Entreprise", "Stagiaire", "Tuteur", "Mail Tuteur", "Téléphone Tuteur", "Année Stage"};
                vue.getModeleTableStage().setColumnIdentifiers(titresColonnes);
                String[] ligneDonnees = new String[8];
                for (Stage unStage : lesStages) {
                    ligneDonnees[0] = unStage.getUneEntreprise().getNom();
                    ligneDonnees[1] = unStage.getUneEntreprise().getAdr() + ", " + Integer.toString(unStage.getUneEntreprise().getCp()) + " " + unStage.getUneEntreprise().getVille();
                    ligneDonnees[2] = unStage.getUneEntreprise().getTel();
                    ligneDonnees[3] = unStage.getUnEleve().getNom() + " " + unStage.getUnEleve().getPrenom();
                    ligneDonnees[4] = unStage.getUnProfessionnel().getNom() + " " + unStage.getUnProfessionnel().getPrenom();
                    ligneDonnees[5] = unStage.getUnProfessionnel().getMail();
                    ligneDonnees[6] = unStage.getUnProfessionnel().getTel();
                    ligneDonnees[7] = Integer.toString(unStage.getAnneeStage());
                    vue.getModeleTableStage().addRow(ligneDonnees);
                }
                //Si la Table Jury ne retourne aucune ligne
                if (vue.getModeleTableStage().getRowCount() == 0) {
                    JOptionPane.showMessageDialog(frame, "Aucune donnée pour ces paramètres", "Erreur", JOptionPane.ERROR_MESSAGE);
                }
                // Redimensionner table en fonction de son contenu
                generalCtrl.resizeColumnWidth(vue.getjTableListeStage());
            } else { //Si Année début supérieur à Année fin
                JOptionPane.showMessageDialog(frame, "Année début supérieur à Année fin", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getVue(), "CtrlListeStage - échec de sélection des stages" + ex);
        }
    }

    // ACCESSEURS et MUTATEURS
    public VueListeStage getVue() {
        return vue;
    }

    public void setVue(VueListeStage vue) {
        this.vue = vue;
    }

    //Méthodes Abstraites (WindowListener)
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        generalCtrl.quitterApplication();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    //Méthode Abstraite (ActionListener)
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonRetour())) {
            generalCtrl.afficherMain();
        }
        if (e.getSource().equals(vue.getjButtonRefresh())) {
            afficherListeStage();
        }
        if (e.getSource().equals(vue.getjRadioButtonSISR())) {
            spe = "SISR";
        }
        if (e.getSource().equals(vue.getjRadioButtonSLAM())) {
            spe = "SLAM";
        }
    }
}
