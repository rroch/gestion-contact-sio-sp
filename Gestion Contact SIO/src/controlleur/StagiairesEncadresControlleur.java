package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modele.dao.StageDAO;
import modele.dao.EntrepriseDAO;
import modele.dao.ProfessionnelDAO;
import modele.metier.Stage;
import modele.metier.Entreprise;
import modele.metier.Professionnel;
import vue.VueStagiairesEncadres;

/**
 * Controlleur StagiairesEncadres
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class StagiairesEncadresControlleur implements WindowListener, ActionListener {

    private VueStagiairesEncadres vue;
    private GeneralControlleur generalCtrl;

    public StagiairesEncadresControlleur(VueStagiairesEncadres vue, GeneralControlleur ctrl) throws SQLException {
        this.vue = vue;
        this.generalCtrl = ctrl;
        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);

        // le contrôleur écoute les actions des boutons
        this.vue.getjButtonRetour().addActionListener(this);
        this.vue.getjButtonRefresh().addActionListener(this);
        this.vue.getjComboBoxOrg().addActionListener(this);
        this.vue.getjComboBoxContact().addActionListener(this);

        // préparer l'état iniitial de la vue
        ArrayList<Professionnel> lesPros = ProfessionnelDAO.selectAll();
        ArrayList<Entreprise> lesEntres = EntrepriseDAO.selectAll();

        if (!lesPros.isEmpty() || !lesEntres.isEmpty()) {
            //Remise à zéro des JComboBox
            this.vue.getjComboBoxContact().setModel(new javax.swing.DefaultComboBoxModel());
            this.vue.getjComboBoxOrg().setModel(new javax.swing.DefaultComboBoxModel());

            //Ajout d'une ligne neutre dans les Combo Box
            this.vue.getjComboBoxContact().addItem("Tous");
            this.vue.getjComboBoxOrg().addItem("Tous");

            //Remplissage des JComboBox avec les années
            for (Professionnel unPro : lesPros) {
                this.vue.getjComboBoxContact().addItem(unPro.getNom());
            }
            for (Entreprise uneEntre : lesEntres) {
                this.vue.getjComboBoxOrg().addItem(uneEntre.getNom());
            }

            //Sélection des Années de début et de fin par défaut
            this.vue.getjComboBoxContact().setSelectedIndex(0);
            this.vue.getjComboBoxOrg().setSelectedIndex(0);

            // Remplissage de la table
            afficherLesStagiaires();
        }
    }

    /*
    * Remplissage de la table
     */
    private void afficherLesStagiaires() {
        ArrayList<Stage> lesStages = null;
        JFrame frame = new JFrame("showMessageDialog");
        try {
            //Éxecute la requête correspondante à la sélection des combos box
            //Si les combos box sont positionnées à Tous, on affiche tous les stagiaires
            if (this.vue.getjComboBoxContact().getSelectedIndex() == 0 && this.vue.getjComboBoxOrg().getSelectedIndex() == 0) {
                lesStages = StageDAO.selectAll();
            }
            //Si la combo box contact n'est pas à Tous, on affiche les stagiaires selon le Professionnel
            if (this.vue.getjComboBoxContact().getSelectedIndex() != 0 && this.vue.getjComboBoxOrg().getSelectedIndex() == 0) {
                lesStages = StageDAO.selectStagiaireByPro(this.vue.getjComboBoxContact().getSelectedItem().toString());
            }
            //Si la combo box organisation n'est pas à Tous, on affiche les stagiaires selon l'Entreprise
            if (this.vue.getjComboBoxContact().getSelectedIndex() == 0 && this.vue.getjComboBoxOrg().getSelectedIndex() != 0) {
                lesStages = StageDAO.selectStagiaireByEntre(this.vue.getjComboBoxOrg().getSelectedItem().toString());
            }
            //Si les deux combos box ne sont pas à Tous, on interdit la recherche
            if (this.vue.getjComboBoxContact().getSelectedIndex() != 0 && this.vue.getjComboBoxOrg().getSelectedIndex() != 0) {
                JOptionPane.showMessageDialog(frame, "Vous ne pouvez pas faire une sélection avec les deux combos box", "Erreur", JOptionPane.ERROR_MESSAGE);
            }

            //On indique le nombre de colonnes de la table et leur nom
            vue.getModeleTableStagiaire().setRowCount(0);
            String[] titresColonnes = {"Tuteur", "Stagiaire", "Spécialité", "Année Stage", "Période Stage", "Sujet Stage", "Technologie étudiée"};
            vue.getModeleTableStagiaire().setColumnIdentifiers(titresColonnes);
            String[] lignesDonnees = new String[7];

            //On remplit la table avec les données de la BDD
            for (Stage unStage : lesStages) {
                lignesDonnees[0] = unStage.getUnProfessionnel().getNom() + " " + unStage.getUnProfessionnel().getPrenom() + " (" + unStage.getUnProfessionnel().getMail() + ")";
                lignesDonnees[1] = unStage.getUnEleve().getNom() + " " + unStage.getUnEleve().getPrenom();
                lignesDonnees[2] = unStage.getUnProfessionnel().getSpe();
                lignesDonnees[3] = Integer.toString(unStage.getAnneeStage());
                lignesDonnees[4] = Integer.toString(unStage.getPeriodeStage()) + " semaines";
                lignesDonnees[5] = unStage.getUnSujetStage();
                lignesDonnees[6] = unStage.getUneTechnologie().getLibelle();
                getVue().getModeleTableStagiaire().addRow(lignesDonnees);
            }
            //Si la Table ne retourne aucune ligne
            if (vue.getModeleTableStagiaire().getRowCount() == 0) {
                JOptionPane.showMessageDialog(frame, "Aucune donnée pour ces paramètres", "Erreur", JOptionPane.ERROR_MESSAGE);
            }

            // Redimensionner table en fonction de son contenu
            generalCtrl.resizeColumnWidth(vue.getjTableStagiaires());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getVue(), "CtrlStagiairesEncadres - échec de sélection des stages");
        }
    }

    // ACCESSEURS et MUTATEURS
    public VueStagiairesEncadres getVue() {
        return vue;
    }

    public void setVue(VueStagiairesEncadres vue) {
        this.vue = vue;
    }

    public GeneralControlleur getGeneralCtrl() {
        return generalCtrl;
    }

    public void setGeneralCtrl(GeneralControlleur generalCtrl) {
        this.generalCtrl = generalCtrl;
    }

    // REACTIONS EVENEMENTIELLES
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        generalCtrl.quitterApplication();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonRetour())) { //Bouton Retour
            generalCtrl.afficherMain();
        }
        if (e.getSource().equals(vue.getjButtonRefresh())) { //Bouton Rechercher / Rafraichir
            afficherLesStagiaires();
        }

        //Si la combo box organisation n'est pas à Tous, on désactive la combo box contact
        if (e.getSource().equals(vue.getjComboBoxOrg())) { //Bouton Rechercher / Rafraichir
            if (vue.getjComboBoxOrg().getSelectedIndex() != 0) {
                vue.getjComboBoxContact().setEnabled(false);
            } else {
                vue.getjComboBoxContact().setEnabled(true);
            }
        }
        //Si la combo box contact n'est pas à Tous, on désactive la combo box organisation
        if (e.getSource().equals(vue.getjComboBoxContact())) { //Bouton Rechercher / Rafraichir
            if (vue.getjComboBoxContact().getSelectedIndex() != 0) {
                vue.getjComboBoxOrg().setEnabled(false);
            } else {
                vue.getjComboBoxOrg().setEnabled(true);
            }
        }
    }
}
