package vue;



import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * Vue Main
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class VueMain extends javax.swing.JFrame {

    /**
     * Creates new form VueMain
     */
    public VueMain() {
        initComponents();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
        this.setTitle("Gestion Contact BTS SIO");
        this.setLocationRelativeTo(null);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonListeStage = new javax.swing.JButton();
        jButtonContactAccordStagiaire = new javax.swing.JButton();
        jButtonContactAccordJury = new javax.swing.JButton();
        jLabelTitre = new javax.swing.JLabel();
        jLabelAPropos = new javax.swing.JLabel();
        jButtonImportCSV = new javax.swing.JButton();
        jButtonStagiairesEncadres = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jButtonListeStage.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonListeStage.setText("Liste des Stages");
        jButtonListeStage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonListeStageActionPerformed(evt);
            }
        });

        jButtonContactAccordStagiaire.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonContactAccordStagiaire.setText("Liste Accord Stagiaire");
        jButtonContactAccordStagiaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonContactAccordStagiaireActionPerformed(evt);
            }
        });

        jButtonContactAccordJury.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonContactAccordJury.setText("Liste Accord Jury");

        jLabelTitre.setFont(new java.awt.Font("Open Sans Light", 1, 36)); // NOI18N
        jLabelTitre.setText("Gestion Contact BTS SIO");

        jLabelAPropos.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabelAPropos.setText("Application créée par Clément Baudry, Loan Lemauff, Romain Roch, Valérian Touzard");

        jButtonImportCSV.setText("Importer CSV");
        jButtonImportCSV.setEnabled(false);

        jButtonStagiairesEncadres.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonStagiairesEncadres.setText("Liste Stagiaires Encadrés");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabelTitre)
                                .addGap(86, 86, 86))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jButtonContactAccordJury, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButtonContactAccordStagiaire, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButtonListeStage, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonStagiairesEncadres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonImportCSV)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelAPropos)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jLabelTitre)
                .addGap(18, 18, 18)
                .addComponent(jButtonListeStage, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonContactAccordStagiaire, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonContactAccordJury, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonStagiairesEncadres, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAPropos)
                    .addComponent(jButtonImportCSV))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonListeStageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonListeStageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonListeStageActionPerformed

    private void jButtonContactAccordStagiaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonContactAccordStagiaireActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonContactAccordStagiaireActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VueMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VueMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VueMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VueMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VueMain().setVisible(true);
            }
        });
    }

    public JButton getjButtonContactAccordJury() {
        return jButtonContactAccordJury;
    }

    public JButton getjButtonContactAccordStagiaire() {
        return jButtonContactAccordStagiaire;
    }

    public JButton getjButtonImportCSV() {
        return jButtonImportCSV;
    }

    public JButton getjButtonListeStage() {
        return jButtonListeStage;
    }

    public JButton getjButtonStagiairesEncadres() {
        return jButtonStagiairesEncadres;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonContactAccordJury;
    private javax.swing.JButton jButtonContactAccordStagiaire;
    private javax.swing.JButton jButtonImportCSV;
    private javax.swing.JButton jButtonListeStage;
    private javax.swing.JButton jButtonStagiairesEncadres;
    private javax.swing.JLabel jLabelAPropos;
    private javax.swing.JLabel jLabelTitre;
    // End of variables declaration//GEN-END:variables
}
