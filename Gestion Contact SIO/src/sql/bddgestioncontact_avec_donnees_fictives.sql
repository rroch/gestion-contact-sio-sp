-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 03 avr. 2020 à 19:24
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `prj2eq10_gestioncontactsio`
--
DROP DATABASE IF EXISTS `prj2eq10_gestioncontactsio`;
CREATE DATABASE prj2eq10_gestioncontactsio CHARACTER SET 'utf8';
USE prj2eq10_gestioncontactsio;
-- --------------------------------------------------------

--
-- Structure de la table `ANNEE`
--
CREATE TABLE IF NOT EXISTS `ANNEE` (
  `DATEANNEE` int(5) NOT NULL,
  PRIMARY KEY (`DATEANNEE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ANNEE`
--

INSERT INTO `ANNEE` (`DATEANNEE`) VALUES
(2012),
(2013),
(2014),
(2015),
(2016),
(2017),
(2018),
(2019),
(2020);

-- --------------------------------------------------------

--
-- Structure de la table `ELEVES`
--

CREATE TABLE IF NOT EXISTS `ELEVES` (
  `IDELEVE` int(6) NOT NULL,
  `NOMELEVE` varchar(30) NOT NULL,
  `PRENOMELEVE` varchar(30) NOT NULL,
  PRIMARY KEY (`idEleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ELEVES`
--

INSERT INTO `ELEVES` (`IDELEVE`, `NOMELEVE`, `PRENOMELEVE`) VALUES
(1, 'BALESTRAT', 'Rudy'),
(2, 'GIRARD ', 'Matheo'),
(3, 'LE FOURN', 'Alex'),
(4, 'RAYER', 'Alex'),
(5, 'MISSIAEN', 'Gaspar'),
(6, 'NICOD', 'Matthias'),
(7, 'PROUX', 'Titouan '),
(8, 'BERNARDEAU', 'Louis'),
(9, 'FRION', 'Arthur'),
(10, 'GIRES', 'William'),
(11, 'CHIFFOLEAU', 'Chloe'),
(12, 'PINEAU', 'Anthony');

-- --------------------------------------------------------

--
-- Structure de la table `ENSEIGNANTS`
--

CREATE TABLE IF NOT EXISTS `ENSEIGNANTS` (
  `IDENSEI` int(3) NOT NULL,
  `NOMENSEI` varchar(30) NOT NULL,
  `PRENOMENSEI` varchar(30) NOT NULL,
  PRIMARY KEY (`IDENSEI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enseignants`
--

INSERT INTO `ENSEIGNANTS` (`IDENSEI`, `NOMENSEI`, `PRENOMENSEI`) VALUES
(1, 'CONTANT', 'Nelly'),
(2, 'SIMON', 'Bernard'),
(3, 'BOURGEOIS', 'Nicolas');

-- --------------------------------------------------------

--
-- Structure de la table `ENTREPRISES`
--

CREATE TABLE IF NOT EXISTS `ENTREPRISES` (
  `IDENTREPRISE` int(10) NOT NULL,
  `NOMENTREPRISE` varchar(30) NOT NULL,
  `NUMTELENTREPRISE` varchar(14) NOT NULL,
  `ADRENTREPRISE` varchar(50) NOT NULL,
  `CPENTREPRISE` int(5) NOT NULL,
  `VILLEENTREPRISE` varchar(30) NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ENTREPRISES`
--

INSERT INTO `ENTREPRISES` (`IDENTREPRISE`, `NOMENTREPRISE`, `NUMTELENTREPRISE`, `ADRENTREPRISE`, `CPENTREPRISE`, `VILLEENTREPRISE`) VALUES
(1, 'Dalmace Company', '03.89.11.26.21', '15, rue Descartes', 67200, 'STRASBOURG'),
(2, 'Mercer OIUT', '03.46.28.65.33', '29, rue de Lille', 59280, 'ARMENTIÈRES'),
(3, 'Masson Company', '02.18.00.68.50', '41, rue de lAigle', 97419, 'LA POSSESSION'),
(4, 'Poulin Company', '01.31.42.11.75', '46, Place de la Madeleine', 75009, 'PARIS'),
(5, 'Berthelette Company', '02.61.66.94.56', '57, rue du Château', 44800, 'SAINT-HERBLAIN'),
(6, 'Lessard Inc.', '02.40.55.99.53', '83, avenue Jules Ferry', 76300, 'SOTTEVILLE-LÈS-ROUEN'),
(7, 'Achin IT', '04.05.35.52.14', '25, Chemin des Bateliers', 73100, 'AIX-LES-BAINS'),
(8, 'InformatikUp', '08.04.06.04.05', '16, Rue du général de Gaulle', 44230, 'SAINT SEBASTIEN SUR LOIRE');

-- --------------------------------------------------------

--
-- Structure de la table `PROFESSIONNELS`
--

CREATE TABLE IF NOT EXISTS `PROFESSIONNELS` (
  `IDPROF` int(5) NOT NULL,
  `NOMPROF` varchar(30) NOT NULL,
  `PRENOMPROF` varchar(30) NOT NULL,
  `SPECIALITEPROF` varchar(5) NOT NULL,
  `MAILPROF` varchar(30) NOT NULL,
  `NUMEROTELPROF` varchar(14) NOT NULL,
  `IDENTREPRISE` int(10) NOT NULL,
  PRIMARY KEY (`IDPROF`),
  KEY `IDENTREPRISE` (`IDENTREPRISE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `PROFESSIONNELS`
--

INSERT INTO `PROFESSIONNELS` (`IDPROF`, `NOMPROF`, `PRENOMPROF`, `SPECIALITEPROF`, `MAILPROF`, `NUMEROTELPROF`, `IDENTREPRISE`) VALUES
(1, 'LAFLEUR', 'Bernard', 'SISR', 'blafleur@gmail.com', '06.78.52.75.75', 1),
(2, 'GASTON', 'Leon', 'SLAM', 'lgaston@gmail.com', '06.78.52.02.85', 2),
(3, 'RIFTON', 'Romain', 'SISR', 'rrifton@gmail.com', '06.78.52.00.01', 2),
(4, 'PONTON', 'Clément', 'SLAM', 'cponton@gmail.com', '06.78.52.15.53', 3),
(5, 'TRASIR', 'Mael', 'SLAM', 'mtrasir@gmail.com', '06.78.52.45.31', 4),
(6, 'TRES', 'Thomas', 'SISR', 'ttres@gmail.com', '06.78.52.12.25', 5),
(7, 'PORTMAN', 'Thomas', 'SLAM', 'tportman@gmail.com', '06.78.52.82.23', 6),
(8, 'BERNARD', 'Florian', 'SISR', 'fbernard@gmail.com', '06.78.52.98.72', 7),
(9, 'SIRT', 'Kevin', 'SISR', 'ksirt@gmail.com', '06.78.52.84.85', 7),
(10, 'ARTIS', 'Arthur', 'SISR', 'aarthis@gmail.com', '06.78.52.02.58', 8);

-- --------------------------------------------------------

--
-- Structure de la table `SPECIALITE`
--

CREATE TABLE IF NOT EXISTS `SPECIALITE` (
  `IDSPECIALITE` int(2) NOT NULL AUTO_INCREMENT,
  `LIBELLESPECIALITE` varchar(5) NOT NULL,
  `DATEANNEE` int(5) NOT NULL,
  `IDELEVE` int(6) NOT NULL,
  PRIMARY KEY (`IDSPECIALITE`),
  KEY `DATEANNEE` (`DATEANNEE`),
  KEY `IDELEVE` (`IDELEVE`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `specialite`
--

INSERT INTO `SPECIALITE` (`IDSPECIALITE`, `LIBELLESPECIALITE`, `DATEANNEE`, `IDELEVE`) VALUES
(1, 'SISR', 2019, 1),
(2, 'SISR', 2019, 2),
(3, 'SISR', 2019, 3),
(4, 'SISR', 2019, 4),
(5, 'SISR', 2019, 5),
(6, 'SISR', 2019, 6),
(7, 'SLAM', 2019, 7),
(8, 'SLAM', 2019, 8),
(9, 'SLAM', 2019, 9),
(10, 'SLAM', 2019, 10),
(11, 'SLAM', 2019, 11),
(12, 'SLAM', 2019, 12),
(13, 'SLAM', 2020, 7),
(14, 'SLAM', 2020, 8),
(15, 'SLAM', 2020, 9),
(16, 'SLAM', 2020, 10),
(17, 'SLAM', 2020, 11),
(18, 'SLAM', 2020, 12),
(19, 'SISR', 2020, 1),
(20, 'SISR', 2020, 2),
(21, 'SISR', 2020, 3),
(22, 'SISR', 2020, 4),
(23, 'SISR', 2020, 5),
(24, 'SLAM', 2020, 6);

-- --------------------------------------------------------

--
-- Structure de la table `STAGES`
--

CREATE TABLE IF NOT EXISTS `STAGES` (
  `IDSTAGE` int(20) NOT NULL,
  `ANNEESTAGE` int(5) NOT NULL,
  `PERIODESTAGE` int(3) NOT NULL,
  `SUJETSTAGE` varchar(250) NOT NULL,
  `IDENTREPRISE` int(10) NOT NULL,
  `IDELEVE` int(6) NOT NULL,
  `IDPROFESSIONNEL` int(10) NOT NULL,
  `IDTECHNO` int(11) NOT NULL,
  PRIMARY KEY (`idStage`),
  KEY `idEntreprise` (`idEntreprise`),
  KEY `idEleve` (`idEleve`),
  KEY `idProfessionnel` (`idProfessionnel`),
  KEY `idTechno` (`idTechno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `STAGES`
--

INSERT INTO `STAGES` (`IDSTAGE`, `ANNEESTAGE`, `PERIODESTAGE`, `SUJETSTAGE`, `IDENTREPRISE`, `IDELEVE`, `IDPROFESSIONNEL`, `IDTECHNO`) VALUES
(1, 2019, 4, 'Programme Java', 2, 5, 1, 1),
(2, 2019, 4, 'Programme PHP', 1, 2, 2, 2),
(3, 2019, 4, 'Programme Java', 2, 3, 3, 1),
(4, 2019, 4, 'Windev', 3, 4, 4, 3),
(5, 2019, 4, 'Paramètrage PC exo', 4, 5, 5, 5),
(6, 2019, 4, 'Powershell Script', 5, 6, 6, 7),
(7, 2019, 4, 'Exo Montage Réseau', 6, 7, 7, 8),
(8, 2019, 4, 'Test Montage Réseau', 7, 8, 8, 8),
(9, 2019, 4, 'Exo Java', 8, 9, 9, 1),
(10, 2019, 4, 'Test Java unitaire', 1, 10, 10, 1),
(11, 2020, 7, 'Java exo unitaire', 2, 11, 1, 1),
(12, 2020, 7, 'java projet', 3, 12, 2, 2),
(13, 2020, 7, 'projet php', 4, 1, 3, 2),
(14, 2020, 7, 'projet Windev', 5, 2, 4, 3),
(15, 2020, 7, 'exo paramétrage', 6, 3, 5, 5),
(16, 2020, 7, 'Test param pc', 7, 4, 6, 5),
(17, 2020, 7, 'java test projet', 8, 5, 7, 1),
(18, 2020, 7, 'php exo test', 1, 6, 8, 2),
(19, 2020, 7, 'script test php', 2, 7, 9, 7);

-- --------------------------------------------------------

--
-- Structure de la table `TECHNOLOGIE`
--

CREATE TABLE IF NOT EXISTS `TECHNOLOGIE` (
  `IDTECH` int(11) NOT NULL,
  `LIBELLETECH` varchar(50) NOT NULL,
  PRIMARY KEY (`IDTECH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `TECHNOLOGIE`
--

INSERT INTO `TECHNOLOGIE` (`IDTECH`, `LIBELLETECH`) VALUES
(1, 'Java'),
(2, 'PHP'),
(3, 'Windesign/Windev'),
(4, 'Symfony'),
(5, 'Paramètrages PC'),
(6, 'Paramètrage Switch'),
(7, 'Powershell'),
(8, 'Montage Réseau');

-- --------------------------------------------------------

--
-- Structure de la table `VISITE`
--

CREATE TABLE IF NOT EXISTS `VISITE` (
  `IDVISITE` int(20) NOT NULL,
  `ACCORDJURY` tinyint(1) NOT NULL,
  `ACCORD1ERESTAGE` tinyint(1) NOT NULL,
  `ACCORD2EMESTAGE` tinyint(1) NOT NULL,
  `IDSTAGE` int(20) NOT NULL,
  `IDENSEI` int(3) NOT NULL,
  PRIMARY KEY (`IDVISITE`),
  KEY `IDSTAGE` (`IDSTAGE`),
  KEY `IDENSEI` (`IDENSEI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `VISITE`
--

INSERT INTO `VISITE` (`IDVISITE`, `ACCORDJURY`, `ACCORD1ERESTAGE`, `ACCORD2EMESTAGE`, `IDSTAGE`, `IDENSEI`) VALUES
(1, 0, 1, 0, 1, 2),
(2, 1, 0, 0, 2, 3),
(3, 1, 1, 1, 3, 2),
(4, 0, 1, 1, 4, 1),
(5, 0, 0, 0, 5, 2),
(6, 1, 1, 1, 6, 3),
(7, 0, 1, 1, 7, 3),
(8, 1, 0, 1, 8, 2),
(9, 1, 0, 0, 1, 2),
(10, 0, 0, 0, 2, 1),
(11, 1, 1, 1, 3, 1),
(12, 0, 1, 1, 4, 1),
(13, 1, 0, 1, 5, 1),
(14, 1, 0, 1, 6, 2),
(15, 1, 0, 0, 7, 1),
(16, 1, 0, 1, 8, 3),
(17, 1, 1, 1, 1, 3),
(18, 0, 0, 0, 2, 2),
(19, 1, 0, 1, 3, 1),
(20, 1, 1, 1, 4, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `PROFESSIONNELS`
--
ALTER TABLE `PROFESSIONNELS`
  ADD CONSTRAINT `professionnels_ibfk_1` FOREIGN KEY (`IDENTREPRISE`) REFERENCES `ENTREPRISES` (`IDENTREPRISE`);

--
-- Contraintes pour la table `SPECIALITE`
--
ALTER TABLE `SPECIALITE`
  ADD CONSTRAINT `specialite_ibfk_1` FOREIGN KEY (`IDELEVE`) REFERENCES `ELEVES` (`IDELEVE`),
  ADD CONSTRAINT `specialite_ibfk_2` FOREIGN KEY (`DATEANNEE`) REFERENCES `ANNEE` (`DATEANNEE`);

--
-- Contraintes pour la table `STAGES`
--
ALTER TABLE `STAGES`
  ADD CONSTRAINT `stages_ibfk_1` FOREIGN KEY (`IDELEVE`) REFERENCES `ELEVES` (`IDELEVE`),
  ADD CONSTRAINT `stages_ibfk_2` FOREIGN KEY (`IDENTREPRISE`) REFERENCES `ENTREPRISES` (`IDENTREPRISE`),
  ADD CONSTRAINT `stages_ibfk_3` FOREIGN KEY (`IDPROFESSIONNEL`) REFERENCES `PROFESSIONNELS` (`IDPROF`),
  ADD CONSTRAINT `stages_ibfk_4` FOREIGN KEY (`IDTECHNO`) REFERENCES `TECHNOLOGIE` (`IDTECH`);

--
-- Contraintes pour la table `VISITE`
--
ALTER TABLE `VISITE`
  ADD CONSTRAINT `visite_ibfk_1` FOREIGN KEY (`IDENSEI`) REFERENCES `ENSEIGNANTS` (`IDENSEI`),
  ADD CONSTRAINT `visite_ibfk_2` FOREIGN KEY (`IDSTAGE`) REFERENCES `STAGES` (`IDSTAGE`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
