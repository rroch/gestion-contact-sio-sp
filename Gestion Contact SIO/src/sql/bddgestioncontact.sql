-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 03 avr. 2020 à 19:24
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `prj2eq10_gestioncontactsio`
--
DROP DATABASE IF EXISTS `prj2eq10_gestioncontactsio`;
CREATE DATABASE prj2eq10_gestioncontactsio CHARACTER SET 'utf8';
USE prj2eq10_gestioncontactsio;
-- --------------------------------------------------------

--
-- Structure de la table `ANNEE`
--

CREATE TABLE IF NOT EXISTS `ANNEE` (
  `DATEANNEE` int(5) NOT NULL,
  PRIMARY KEY (`DATEANNEE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ELEVES`
--

CREATE TABLE IF NOT EXISTS `ELEVES` (
  `IDELEVE` int(6) NOT NULL,
  `NOMELEVE` varchar(30) NOT NULL,
  `PRENOMELEVE` varchar(30) NOT NULL,
  PRIMARY KEY (`IDELEVE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ENSEIGNANTS`
--

CREATE TABLE IF NOT EXISTS `ENSEIGNANTS` (
  `IDENSEI` int(3) NOT NULL,
  `NOMENSEI` varchar(30) NOT NULL,
  `PRENOMENSEI` varchar(30) NOT NULL,
  PRIMARY KEY (`IDENSEI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ENTREPRISES`
--

CREATE TABLE IF NOT EXISTS `ENTREPRISES` (
  `IDENTREPRISE` int(10) NOT NULL,
  `NOMENTREPRISE` varchar(30) NOT NULL,
  `NUMTELENTREPRISE` varchar(14) NOT NULL,
  `ADRENTREPRISE` varchar(50) NOT NULL,
  `CPENTREPRISE` int(5) NOT NULL,
  `VILLEENTREPRISE` varchar(30) NOT NULL,
  PRIMARY KEY (`IDENTREPRISE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PROFESSIONNELS`
--

CREATE TABLE IF NOT EXISTS `PROFESSIONNELS` (
  `IDPROF` int(5) NOT NULL,
  `NOMPROF` varchar(30) NOT NULL,
  `PRENOMPROF` varchar(30) NOT NULL,
  `SPECIALITEPROF` varchar(5) NOT NULL,
  `MAILPROF` varchar(30) NOT NULL,
  `NUMEROTELPROF` varchar(14) NOT NULL,
  `IDENTREPRISE` int(10) NOT NULL,
  PRIMARY KEY (`IDPROF`),
  KEY `IDENTREPRISE` (`IDENTREPRISE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SPECIALITE`
--

CREATE TABLE IF NOT EXISTS `SPECIALITE` (
  `IDSPECIALITE` int(2) NOT NULL AUTO_INCREMENT,
  `LIBELLESPECIALITE` varchar(5) NOT NULL,
  `DATEANNEE` int(5) NOT NULL,
  `IDELEVE` int(6) NOT NULL,
  PRIMARY KEY (`IDSPECIALITE`),
  KEY `DATEANNEE` (`DATEANNEE`),
  KEY `IDELEVE` (`IDELEVE`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `STAGES`
--

CREATE TABLE IF NOT EXISTS `STAGES` (
  `IDSTAGE` int(20) NOT NULL,
  `ANNEESTAGE` int(5) NOT NULL,
  `PERIODESTAGE` int(3) NOT NULL,
  `SUJETSTAGE` varchar(250) NOT NULL,
  `IDENTREPRISE` int(10) NOT NULL,
  `IDELEVE` int(6) NOT NULL,
  `IDPROFESSIONNEL` int(10) NOT NULL,
  `IDTECHNO` int(11) NOT NULL,
  PRIMARY KEY (`IDSTAGE`),
  KEY `IDENTREPRISE` (`IDENTREPRISE`),
  KEY `IDELEVE` (`IDELEVE`),
  KEY `IDPROFESSIONNEL` (`IDPROFESSIONNEL`),
  KEY `IDTECHNO` (`IDTECHNO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `TECHNOLOGIE`
--

CREATE TABLE IF NOT EXISTS `TECHNOLOGIE` (
  `IDTECH` int(11) NOT NULL,
  `LIBELLETECH` varchar(50) NOT NULL,
  PRIMARY KEY (`IDTECH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `VISITE`
--

DROP TABLE IF EXISTS `VISITE`;
CREATE TABLE IF NOT EXISTS `VISITE` (
  `IDVISITE` int(20) NOT NULL,
  `ACCORDJURY` tinyint(1) NOT NULL,
  `ACCORD1ERESTAGE` tinyint(1) NOT NULL,
  `ACCORD2EMESTAGE` tinyint(1) NOT NULL,
  `IDSTAGE` int(20) NOT NULL,
  `IDENSEI` int(3) NOT NULL,
  PRIMARY KEY (`IDVISITE`),
  KEY `IDSTAGE` (`IDSTAGE`),
  KEY `IDENSEI` (`IDENSEI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `PROFESSIONNELS`
--
ALTER TABLE `PROFESSIONNELS`
  ADD CONSTRAINT `professionnels_ibfk_1` FOREIGN KEY (`IDENTREPRISE`) REFERENCES `ENTREPRISES` (`IDENTREPRISE`);

--
-- Contraintes pour la table `SPECIALITE`
--
ALTER TABLE `SPECIALITE`
  ADD CONSTRAINT `specialite_ibfk_1` FOREIGN KEY (`IDELEVE`) REFERENCES `ELEVES` (`IDELEVE`),
  ADD CONSTRAINT `specialite_ibfk_2` FOREIGN KEY (`DATEANNEE`) REFERENCES `ANNEE` (`DATEANNEE`);

--
-- Contraintes pour la table `STAGES`
--
ALTER TABLE `STAGES`
  ADD CONSTRAINT `stages_ibfk_1` FOREIGN KEY (`IDELEVE`) REFERENCES `ELEVES` (`IDELEVE`),
  ADD CONSTRAINT `stages_ibfk_2` FOREIGN KEY (`IDENTREPRISE`) REFERENCES `ENTREPRISES` (`IDENTREPRISE`),
  ADD CONSTRAINT `stages_ibfk_3` FOREIGN KEY (`IDPROFESSIONNEL`) REFERENCES `PROFESSIONNELS` (`IDPROF`),
  ADD CONSTRAINT `stages_ibfk_4` FOREIGN KEY (`IDTECHNO`) REFERENCES `TECHNOLOGIE` (`IDTECH`);

--
-- Contraintes pour la table `VISITE`
--
ALTER TABLE `VISITE`
  ADD CONSTRAINT `visite_ibfk_1` FOREIGN KEY (`IDENSEI`) REFERENCES `ENSEIGNANTS` (`IDENSEI`),
  ADD CONSTRAINT `visite_ibfk_2` FOREIGN KEY (`IDSTAGE`) REFERENCES `STAGES` (`IDSTAGE`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
